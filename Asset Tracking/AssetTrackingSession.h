//
//  AssetTrackingSession.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AssetTrackingOrganisation.h"
#import "AssetTrackingItem.h"
#import "AssetTrackingActivityItem.h"
#import "AssetTrackingLeaderBoardItem.h"
#import "AssetTrackingUnitItem.h"
@interface AssetTrackingSession : NSObject
    @property (strong, nonatomic) NSDate *stopTracking;
    @property (strong, nonatomic) NSString *userid;
    @property (strong, nonatomic) NSString *clubId;
    @property (strong, nonatomic) NSString *beachId;
    @property (strong, nonatomic) NSString *password;
    @property (strong, nonatomic) NSString *sessionguid;
    @property (strong, nonatomic) NSString *firstName;
    @property (strong, nonatomic) NSString *lastName;
    @property (strong, nonatomic) AssetTrackingActivityItem *lastActivity;
    @property (strong, nonatomic) AssetTrackingLeaderBoardItem *topLeader;
@property (strong, nonatomic) AssetTrackingUnitItem *currentTrackingUnit;
    @property (strong, nonatomic) NSMutableArray *organisations;
    @property (strong, nonatomic) NSMutableArray *allLocationsPoints;
    @property (strong, nonatomic) NSMutableArray *units;
    @property (strong, nonatomic) NSMutableArray *assets;
    @property (strong, nonatomic) NSMutableArray *hazards;
    @property (strong, nonatomic) NSMutableArray *incidents;
    @property (strong, nonatomic) NSMutableArray *itemUpdateQueue;
   @property (strong, nonatomic) NSMutableArray *activities;
 @property (strong, nonatomic) NSMutableArray *leaders;
@property (strong, nonatomic) AssetTrackingOrganisation* selectedOrganisation;
@property  float longField;
@property  float latField;
@property  float accuracy;
@property  (strong, nonatomic) NSDate* lastGPSDate ;
@property  (strong, nonatomic) NSDate* stopGPSDate ;
 @property (strong, nonatomic)  AssetTrackingOrganisation* selectedAssetTrackingOrganisation;
-(AssetTrackingSession* ) init;
@end

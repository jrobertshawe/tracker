//
//  AssetTrackingLeaderBoardItem.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 5/07/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssetTrackingLeaderBoardItem : NSObject
@property (strong, nonatomic)  NSString *recordid;
@property (strong, nonatomic) NSString *beachName;
@property (strong, nonatomic) NSString *clubName;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *numberOfUpdates;
@property (strong, nonatomic) NSDate *updatedDate;
@property (strong, nonatomic) NSString *imageFile;
@property (strong, nonatomic) NSString *points;
@property (strong, nonatomic) NSString *rank;
@end

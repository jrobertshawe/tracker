//
//  MathUtility.h
//  AliveExamples
//
//  Created by Jeroen Verbeek on 9/08/12.
//  Copyright (c) 2012 Alive. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MAKE_DEGREE_MULTIPLY	(float)57.29577951308232f

#pragma mark - C function prototypes.

/*---------------------------------------------------------------------------------------------*/
/** 
 @brief Find the distance between 2 points
 @param one First point.
 @param two Second point.
 @return The distance between the 2 points.
 @author Jeroen Verbeek
 */
extern float FindDistanceBetween2Points2D(CGPoint one, CGPoint two);

/*---------------------------------------------------------------------------------------------*/
/**
 @brief Find the angle between 2 lines.
 @param point 
 @param offset
 @param result Pointer to a point that will hold the result.
 @return See result pointer parameter.
 @author Jeroen Verbeek
 */
extern void FindAngleBetween2Lines2D(CGPoint point, CGSize offset, CGPoint *result);

#pragma mark END

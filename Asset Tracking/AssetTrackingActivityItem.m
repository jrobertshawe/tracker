//
//  AssetTrackingActivityItem.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingActivityItem.h"

@implementation AssetTrackingActivityItem
@synthesize recordid;
@synthesize beachName;
@synthesize orgName;
@synthesize userName;
@synthesize itemName;
@synthesize createdDate;
@synthesize imageFile;
@end

//
//  AssetTrackingTrackMeViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 22/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AssetTrackingUnitItem.h"
@interface AssetTrackingTrackMeViewController : UIViewController
{
    UITapGestureRecognizer *_tap;
	UIPanGestureRecognizer *_pan;
	CGPoint _lastResult;
        AssetTrackingUnitItem* object;
    UIButton *myButton;
        NSTimer *timer;

}
@property  (nonatomic, retain)  IBOutlet UIImageView *compassBack;
@property  (nonatomic, retain) IBOutlet UIView *compassCover;
@property  (nonatomic, retain) IBOutlet UIImageView *compassArrow;
@property  (nonatomic, retain)  IBOutlet UILabel *compassSelectionName;

-(void)createView;
-(void)updateDisplayWithPoint:(CGPoint)point;
-(void)handleTap:(UIGestureRecognizer*)gesture;
-(void)handlePan:(UIGestureRecognizer*)gesture;
-(void)setupData:(AssetTrackingUnitItem*)pObject;
- (IBAction)ButtonPressed:(id)sender;
-(void)updateTime;
- (void)stopTimer;
@end

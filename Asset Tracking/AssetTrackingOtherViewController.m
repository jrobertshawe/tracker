k;//
//  AssetTrackingOtherViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/08/12.
//
//

#import "AssetTrackingOtherViewController.h"
#import "AssetTrackingAppDelegate.h"
@interface AssetTrackingOtherViewController ()

@end

@implementation AssetTrackingOtherViewController
@synthesize description;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // Do any additional setup after loading the view.
    //change back button image
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    

    
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"img_topbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    [description becomeFirstResponder];
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
}
-(IBAction) editingChanged:(UITextField*)sender
{
    if (sender == description)
    {
        NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890 ."];
        // allow only alphanumeric chars
        NSString* newStr = [sender.text stringByTrimmingCharactersInSet:[s invertedSet]];
        
        if ([newStr length] < [sender.text length])
        {
            sender.text = newStr;
        }
    }
}
- (IBAction)savedPressed:(id)sender
{
    if ([description.text length] == 0)
        return;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AssetTrackingPointsViewControllerOther" object:description.text userInfo:nil];
    
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void) didTapBackButton:(id)sender {
     [[NSNotificationCenter defaultCenter] postNotificationName:@"AssetTrackingPointsViewControllerOther" object:nil userInfo:nil];
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

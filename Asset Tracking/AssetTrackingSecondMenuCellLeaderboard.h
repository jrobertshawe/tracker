//
//  AssetTrackingLeaderBoardCell.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 19/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetTrackingSecondMenuCellLeaderboard : UITableViewCell

@property (strong, nonatomic)   IBOutlet UILabel *menuName;
@property (strong, nonatomic)   IBOutlet UILabel *beachName;
@property (strong, nonatomic)   IBOutlet UILabel *lastupdatedDate;


-(void)setupData:(NSString*) menuText;

@end

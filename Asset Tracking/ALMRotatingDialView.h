//
//  ALMRotatingDialView
//  
//
//  Created by Jeroen Verbeek on 24/08/12.
//  Copyright (c) 2012 Alive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingUnitItem.h"
#define kDefaultInnerLimit 80
#define kDefaultOuterLimit 140

#define kDegreesToRadiansMult (M_PI/180.0)

@interface ALMRotatingDialView : UIView

/** The angle of the dial from 0.0 to 360.0. */
@property (readwrite, nonatomic, setter=customSetDialAngle:, getter=customGetDialAngle) float dialAngle;

/** The value of the dial from 0.0 to 1.0. */
@property (readwrite, nonatomic, setter=customSetDialValue:, getter=customGetDialValue) float dialValue;

/** Set inner and outer limits to simulate a ring of movement. */
@property (readwrite, nonatomic) float outerLimit;
@property (readwrite, nonatomic) float innerLimit;
-(void)setInactive;
-(void)setActive;
-(void)updateTime:(NSTimeInterval)secondsBetween;
-(void)setupData:(AssetTrackingUnitItem*)pObject;
- (IBAction)ButtonPressed:(id)sender;
@end

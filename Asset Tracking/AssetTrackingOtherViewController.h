//
//  AssetTrackingOtherViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/08/12.
//
//

#import <UIKit/UIKit.h>

@interface AssetTrackingOtherViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *description;
-(IBAction) editingChanged:(UITextField*)sender;
- (IBAction)savedPressed:(id)sender;
@end

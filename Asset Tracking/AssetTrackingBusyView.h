//
//  AssetTrackingBusyView.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 24/08/12.
//
//

#import <UIKit/UIKit.h>

@interface AssetTrackingBusyView : UIView
@property (strong, nonatomic)   IBOutlet UIImageView *unitActive;
-(void) show;
-(void) hide;
@end

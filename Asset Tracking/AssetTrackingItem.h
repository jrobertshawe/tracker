//
//  AssetTrackingHazard.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 6/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface AssetTrackingItem : NSObject
@property (strong, nonatomic) NSNumber *locationPointID;
@property (strong, nonatomic) NSString *locationPointCode;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSDate *updatedDate;
@property (strong, nonatomic) NSString *updateBy;
@property (strong, nonatomic) NSString *queueName;
@property int marked;
@property bool sentToServer;
@end

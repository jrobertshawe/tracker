//
//  AssetTrackingAppDelegate.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingSession.h"
#import <CoreData/CoreData.h>
#import "GetCurrerntLocation.h"
#import <CoreLocation/CoreLocation.h>
#import "AssetTrackingLogin.h"
#import "AssetTrackingUploadFile.h"
#define MENUASSET 0
#define MENUHAZARD 1
#define MENUINCIDENT 2
#define MENULEADERBOARD 0
#define MENUACTIVITY 1
#define IDLE 0
#define SENDING 1
#define SENT 2
#define FAILED 3
#define kServerName	@"tracking-test.slsa.asn.au"
//#define kServerName	@"kiwiju.no-ip.info:8080"
#define FOOTERYPOS 416
#define FOOTERHEIGHT 108
#define isIPhone5 ([[UIScreen mainScreen] bounds].size.height == 568)

//#define kServerName	@"kiwiju.no-ip.info:8080"

@protocol ALMOperationDelegate

@required
-(void)operationResult:(id)result;

@end

@interface ALMOperation : NSOperation
{
	double _delayTime;
	NSString *_jsonText;
	NSDictionary *_jsonDictionary;
	NSObject<ALMOperationDelegate> *_delegate;
}

@property (readwrite) double delayTime;
@property (readwrite, retain) NSString *jsonText;
@property (readwrite, retain) NSDictionary *jsonDictionary;
@property (readwrite, retain) NSObject<ALMOperationDelegate> *delegate;

@end

static inline double radians (double degrees) {return degrees * M_PI/180;}

@interface AssetTrackingAppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>
{
    NSManagedObjectContext *managedObjectContext_;
    NSManagedObjectModel *managedObjectModel_;
    NSPersistentStoreCoordinator *persistentStoreCoordinator_;
 
    AssetTrackingSession *assetTrackingSession; 
    GetCurrerntLocation *locationController;
    CLLocationManager *locationManager;
    NSMutableArray* trackingQueue;
    NSLock *netLock;
     NSLock *arrayLock;
  
}
@property (strong, nonatomic) AssetTrackingUploadFile *assetTrackingUploadFile;
@property (strong, nonatomic) AssetTrackingLogin *assetTrackingLogin;
@property (strong, nonatomic) MKNetworkOperation *flOperation;
@property (strong, nonatomic) MKNetworkEngine* networkEngine;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AssetTrackingSession *assetTrackingSession;
- (NSString *)applicationDocumentsDirectory;
- (void)saveContext;
-(void)LoadData;
-(void)StartTracking;
-(void)StopTracking;
- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
- (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize;
- (void)GetLocationPoints;
- (void)GetTrackingPoints;
- (void)uploadFile:(NSString*) fullPath filename:(NSString*) filename;
@end

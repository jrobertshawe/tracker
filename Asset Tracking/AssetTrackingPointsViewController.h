//
//  AssetTrackingPointsViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingSendPointUpdates.h"
#import "AssetTrackingBusyView.h"
@interface AssetTrackingPointsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSUInteger menuIndex;
    
}
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (strong, nonatomic) AssetTrackingSendPointUpdates *assetTrackingSendPointUpdates;
@property (strong, nonatomic) MKNetworkOperation *flOperation;
-(void)setupData:(NSUInteger) menuIndex;
-(void)handleNotificationOther:(NSNotification *)pNotification;
@end

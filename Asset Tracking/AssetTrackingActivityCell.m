//
//  AssetTrackingActivityCell.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingActivityCell.h"
#import "AssetTrackingAppDelegate.h"
@implementation AssetTrackingActivityCell
@synthesize beachName;
@synthesize userName;
@synthesize itemName;
@synthesize createdDate;
@synthesize beachImage;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupData:(AssetTrackingActivityItem*) assetTrackingActivityItem
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    beachName.text = assetTrackingActivityItem.beachName;
  //  userName.text = assetTrackingActivityItem.userName;
    itemName.text = assetTrackingActivityItem.itemName;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    createdDate.text = [formatter stringFromDate:assetTrackingActivityItem.createdDate];
     self.beachImage.image  = [UIImage imageNamed:@"imagebackground.png"];
    NSRange range = [assetTrackingActivityItem.imageFile rangeOfString:@"http"
                                                                  options:NSCaseInsensitiveSearch];
    if(range.location != NSNotFound) {
        [delegate.networkEngine imageAtURL:[NSURL URLWithString:assetTrackingActivityItem.imageFile] onCompletion:^(UIImage *fetchedImage, NSURL *fetchedURL, BOOL isInCache) {
            self.beachImage.image = fetchedImage;
        }];
    }
    else
    {
        if (assetTrackingActivityItem.imageFile != nil && [assetTrackingActivityItem.imageFile length] > 0)
            self.beachImage.image = [UIImage imageWithContentsOfFile:assetTrackingActivityItem.imageFile];
    }
}
@end

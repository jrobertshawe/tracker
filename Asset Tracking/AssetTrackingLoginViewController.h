//
//  AssetTrackingLoginViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingLogin.h"
#import "AssetTrackingBusyView.h"
@interface AssetTrackingLoginViewController : UIViewController<UIWebViewDelegate>
{
    IBOutlet UITextField *userid;
    IBOutlet UITextField *password;	
    IBOutlet UIActivityIndicatorView *loggingonActivity;
    
    
} 
@property (strong, nonatomic)  IBOutlet UIActivityIndicatorView *loggingonActivity;
@property (strong, nonatomic) IBOutlet UITextField *userid;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) AssetTrackingLogin *assetTrackingLogin;
@property (strong, nonatomic) MKNetworkOperation *flOperation;
@property (strong,nonatomic) IBOutlet UIButton* loginButton;
@property (strong,nonatomic) IBOutlet AssetTrackingBusyView* busyScreen;
@property (strong,nonatomic) IBOutlet UIWebView* helpText;
- (IBAction)loginPressed:(id)sender;
- (IBAction)startTracking:(id)sender;
- (IBAction)stopTracking:(id)sender;
- (void)userLoggedOn;
- (void)GetOrgs;
- (IBAction) forgotDetails:(id)sender;
@end

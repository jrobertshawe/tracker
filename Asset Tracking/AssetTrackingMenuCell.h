//
//  AssetTrackingMenuCell.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetTrackingMenuCell : UITableViewCell
@property (strong, nonatomic)   IBOutlet UILabel *menuName;
-(void)setupData:(NSString*) menuText;
@end

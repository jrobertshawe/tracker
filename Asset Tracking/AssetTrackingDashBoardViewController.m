//
//  AssetTrackingDashBoardViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 14/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingDashBoardViewController.h"
#import "AssetTrackingAppDelegate.h"
#import "AssetTrackingCellDashboard.h"
@interface AssetTrackingDashBoardViewController ()

@end

@implementation AssetTrackingDashBoardViewController
@synthesize loggingonActivity;
@synthesize gpsTracker;
@synthesize assetTrackingLogin = _assetTrackingLogin;
@synthesize flOperation = _flOperation;
@synthesize _tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    showorgSelection = NO;
    showgpsTracker = NO;
	
     AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleGPSNotification:)
     name:@"GPSTrackingApp"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleBeachSafeNotification:)
     name:@"BeachSafe"
     object:nil];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
     [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    if ( [delegate.assetTrackingSession.organisations count] == 0)
    {
               
    }
    else if ( [delegate.assetTrackingSession.organisations count] == 1)
    {
        delegate.assetTrackingSession.selectedAssetTrackingOrganisation = [delegate.assetTrackingSession.organisations objectAtIndex:0];
        showgpsTracker = YES;
         
    }
    else
    {
        showgpsTracker = YES;
        showorgSelection = YES;
        
    }
    UIImage *image;
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        if (delegate.assetTrackingSession.accuracy > 0 && delegate.assetTrackingSession.accuracy < 11 && delegate.assetTrackingSession.latField < 0)
            image = [UIImage imageNamed:@"navbarOn.png"];
        else
            image = [UIImage imageNamed:@"img_topbar"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }


}
-(void) viewWillAppear:(BOOL)animated
{

    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"img_topbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
  
        
    }
}


- (void) didTapBackButton:(id)sender {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    delegate.assetTrackingSession = nil;
}
-(void)handleGPSNotification:(NSNotification *)pNotification
{
    
    
    if ( showorgSelection == YES)
        [self performSegueWithIdentifier: @"SelectOrgSegue" sender: self];
    else
    {
        [self performSegueWithIdentifier: @"ShowBeachMenu" sender: self];
    }
}
-(void)handleBeachSafeNotification:(NSNotification *)pNotification
{
    
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://beachsafe.org.au"]];
 
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Table view data source
-(float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSUInteger row = [indexPath row];

    
    if (row == 0)
    {
        return 98;
    }
    else
    {
         return 89;
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
   
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    

    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *tablecell = nil;
    static NSString *CellIdentifier;
    NSUInteger row = [indexPath row];
    NSString *button1Image = nil;
    NSString *button1SelectedImage = nil;
    NSString *button2SelectedImage = nil;
    NSString *button2Image = nil;
    NSString *button1Queue = nil;
    NSString *button2Queue = nil;
       CellIdentifier = @"dashboardCell";
    if (row == 0)
    {
     CellIdentifier = @"TopdashboardCell";
        button1Image = @"but_beachsafe_on.png";
        button1SelectedImage = @"but_beachsafe_off.png";
        button1Queue = @"BeachSafe";
        if (showgpsTracker)
        {
            button2Image = @"but_patrol_off.png";
            button2SelectedImage = @"but_patrol_on.png";
            button2Queue = @"GPSTrackingApp";
        }
        else
            button2Image = nil;
    }
    else  if (row == 1)
    {
        
        CellIdentifier = @"dashboardCell";
        
        button2Image = nil;
    }
        
        
        
        
        AssetTrackingCellDashboard *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[AssetTrackingCellDashboard alloc] initWithStyle:UITableViewCellStyleSubtitle
                                                reuseIdentifier:CellIdentifier];
        }
    [cell setupData:button1Image rightImage:button2Image leftButtonSelectedImage:button1SelectedImage rightSelectedImage:button2SelectedImage leftQueueName:button1Queue rightQueueName:button2Queue];

    tablecell = cell;      
    return tablecell;
}
@end

//
//  AssetTrackingPointCell.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 14/08/12.
//
//

#import "AssetTrackingPointCell.h"
#import "AssetTrackingAppDelegate.h"
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CAMediaTimingFunction.h>
@implementation AssetTrackingPointCell
@synthesize pointName;
@synthesize updateBy;
@synthesize sign;
@synthesize updateDate;
@synthesize cellBackground;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void) CreateNotification:(NSString *)queuename
{
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:queuename
     object:nil];
}
-(void) showBusy
{
     [sign setImage:[UIImage imageNamed:@"img_loading.png"]];
    
    [self spinLayer:sign.layer duration:.5 direction:1];
}
-(void)handleNotification:(NSNotification *)pNotification
{
   
    if (entity.marked == SENDING)
    {
         [self showBusy];
        cellBackground.image = [UIImage imageNamed:@"but_navbar_on.png"];
        updateDate.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        updateBy.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        pointName.text  = entity.locationPointCode;
        pointName.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        
        if (entity.updateBy != nil)
            updateBy.text = entity.updateBy;
        //    [sign setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_on",entityObject.locationPointCode]]];
    }
    else if (entity.marked == FAILED)
    {
         [self hideBusy];
        cellBackground.image = [UIImage imageNamed:@"but_navbar_off.png"];
        updateDate.textColor =  [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
        updateBy.textColor =  [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
        pointName.textColor = [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
        pointName.text  = entity.locationPointCode;
        if (entity.updateBy != nil)
            updateBy.text = entity.updateBy;
        [sign setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",entity.locationPointCode]]];
    }
    else if (entity.marked == SENT)
    {
        [self hideBusy];
        cellBackground.image = [UIImage imageNamed:@"but_navbar_on.png"];
        updateDate.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        updateBy.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        pointName.text  = entity.locationPointCode;
        pointName.textColor = [UIColor colorWithRed:.4 green:1 blue:.4 alpha:1];
        pointName.text  = entity.locationPointCode;
        if (entity.updateBy != nil)
            updateBy.text = entity.updateBy;
            [sign setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_on",entity.locationPointCode]]];
    }


}
-(void) hideBusy
{
   
    
    
    [sign.layer removeAllAnimations];
     [sign setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_on",entity.locationPointCode]]];
}
-(void)setupData:(AssetTrackingItem*) entityObject
{
    entity = entityObject;
    pointName.text  = entity.locationPointCode;
    if (entity.updateBy != nil)
        updateBy.text = entity.updateBy;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterMediumStyle];
        updateDate.text = [formatter stringFromDate:entityObject.updatedDate];
    if (entity.marked == SENDING)
    {
        [self showBusy];
        cellBackground.image = [UIImage imageNamed:@"but_navbar_on.png"];
        updateDate.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        updateBy.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        pointName.textColor = [UIColor colorWithRed:.4 green:1 blue:.4 alpha:1];
        
        if (entity.updateBy != nil)
            updateBy.text = entity.updateBy;
        //    [sign setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_on",entityObject.locationPointCode]]];
    }
    else if (entity.marked == FAILED)
    {
        [self hideBusy];
        cellBackground.image = [UIImage imageNamed:@"but_navbar_off.png"];
        updateDate.textColor =  [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
        updateBy.textColor =  [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
        pointName.textColor = [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
     
      
    }
    else if (entity.marked == SENT)
    {
        [self hideBusy];
        cellBackground.image = [UIImage imageNamed:@"but_navbar_on.png"];
        updateDate.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        updateBy.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        pointName.textColor = [UIColor colorWithRed:.4 green:1 blue:.4 alpha:1];
        if (entity.updateBy != nil)
            updateBy.text = entity.updateBy;
        [sign setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_on",entity.locationPointCode]]];
    }
    else
    {
         [self hideBusy];
        [sign setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",entity.locationPointCode]]];
        cellBackground.image = [UIImage imageNamed:@"but_navbar_off.png"];
        updateDate.textColor =  [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
        updateBy.textColor =  [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
        pointName.textColor = [UIColor colorWithRed:.75 green:.75 blue:.75 alpha:1   ];
    }


}
- (void)spinLayer:(CALayer *)inLayer duration:(CFTimeInterval)inDuration
        direction:(int)direction
{
    CABasicAnimation* rotationAnimation;
    
    // Rotate about the z axis
    rotationAnimation =
    [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    // Rotate 360 degress, in direction specified
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * direction];
    
    // Perform the rotation over this many seconds
    rotationAnimation.duration = inDuration;
    rotationAnimation.repeatCount = HUGE_VALF;
    // Set the pacing of the animation
    rotationAnimation.timingFunction =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    // Add animation to the layer and make it so
    [inLayer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end

//
//  AssetTrackingBeachSafetyViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingBeachSafetyViewController.h"
#import "AssetTrackingAppDelegate.h"
@interface AssetTrackingBeachSafetyViewController ()

@end

@implementation AssetTrackingBeachSafetyViewController
@synthesize webView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  	// Do any additional setup after loading the view.
    //change back button image
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"img_topbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
  
        
    }
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
    [webView setBackgroundColor:[UIColor clearColor]];
    [webView setOpaque:NO];
    
    NSString* plainContent = @"<p><big><b>Always swim between the red and yellow flags</b></big></p>  <p> When you see red and yellow flags on a beach it indicates that there is currently a lifesaving service operating on the beach. The lifeguards have chosen a section of the beach that is best for swimming and they will closely supervise this area. Lifeguards pay more attention to the area between the red and yellow flags than any other part of the beach.        </p>  <p><big><b> Read the safety signs</b></big>        </p>  <p> Before you go on to the beach be sure to read the safety signs. This will ensure you are aware of any warnings or dangers on the beach. You can also find other helpful information to make your day at the beach more enjoyable. You might also find single signs placed on the beach to highlight specific warnings.       </p>  <p><big><b> Ask a lifeguard for safety advice</b></big>            </p>  <p> Lifeguards are highly trained and very knowledgeable about beach safety and conditions. When you arrive at the beach look for and identify the lifeguards. Feel free to ask them about the day’s conditions, as well any additional beach safety advice they might have for that specific beach – because every beach is different.               </p>  <p><big><b> Swim with a friend</b></big>   </p>  <p> Not only is swimming with a friend (or family member) a fun way to enjoy the beach, it is also very sensible. While you are swimming together you can keep an eye out for each other, and if further assistance was required, one person could call or go for help. If everyone swimming together knows their own limits it is a good idea to let the others know so you can all stay within everyone’s comfortable limits.   </p>  <p><big><b> If you need help, stay calm and attract attention</b></big></p>  <p> Even the most careful people can find themselves out of their limits. If you are not feeling comfortable in the water and you require assistance by a lifeguard to get back to shore, stay calm, raise your arm in the air and wave it from side to side. This will attract the attention of a lifeguard who will be able to come to your assistance. You should conserve your energy by floating on your back and staying calm, this will  ensure you have the energy to remain afloat until assistance arrives.</p>";
    NSString* htmlContentString = [NSString stringWithFormat:
                                   @"<html>"
                                   "<body style=\"color:#999999;font-family:calibri,sans-serif;font-size:14;\">"
                                   "<p>%@</p>"
                                   "</body></html>", plainContent];
    
    [webView loadHTMLString:htmlContentString baseURL:nil];
    
    
}



- (void) didTapBackButton:(id)sender {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end

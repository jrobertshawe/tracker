//
//  AssetTrackingOrgCell.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 6/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingOrgCell.h"
#import "AssetTrackingAppDelegate.h"

@implementation AssetTrackingOrgCell
@synthesize orgName;
@synthesize beachName;
@synthesize beachImage;
@synthesize points;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];


}
-(void)setupData:(AssetTrackingOrganisation*) entityObject queueName:(NSString*) queueName
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    orgName.text  = entityObject.orgName;
   beachName.text  = entityObject.beachName;
    points.text = [NSString stringWithFormat:@"%@ (%@)",entityObject.rank,entityObject.points   ];
    queue = queueName;
    object = entityObject;
    self.beachImage.image  = [UIImage imageNamed:@"imagebackground.png"];
    if (entityObject.imageFile != nil && [entityObject.imageFile isKindOfClass:[NSString class]])
    {
        NSRange range = [entityObject.imageFile rangeOfString:@"http"
                                          options:NSCaseInsensitiveSearch];
        if(range.location != NSNotFound) {
            [delegate.networkEngine imageAtURL:[NSURL URLWithString:entityObject.imageFile] onCompletion:^(UIImage *fetchedImage, NSURL *fetchedURL, BOOL isInCache) {
                self.beachImage.image = fetchedImage;
            }];
        }
        else
        {
            self.beachImage.image = [UIImage imageWithContentsOfFile:entityObject.imageFile];
        }
    }
   
}
- (IBAction)MenuButtonPressed:(id)sender
{
        [self setSelected:YES];	
    if (queue != nil)
        [[NSNotificationCenter defaultCenter] postNotificationName:queue object:object userInfo:nil];

}
@end

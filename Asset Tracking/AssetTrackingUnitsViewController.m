//
//  AssetTrackingUnitsViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 21/08/12.
//
//

#import "AssetTrackingUnitsViewController.h"
#import "AssetTrackingAppDelegate.h"
#import "AssetTrackingUnitCell.h"

@interface AssetTrackingUnitsViewController ()

@end

@implementation AssetTrackingUnitsViewController
@synthesize _tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    [super viewDidLoad];
    //change back button image
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
   
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleRefresh:)
     name:@"RefreshDisplay"
     object:nil];
    UIImage *image;
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        if (delegate.assetTrackingSession.accuracy > 0 && delegate.assetTrackingSession.accuracy < 11 &&  delegate.assetTrackingSession.latField < 0)
            image = [UIImage imageNamed:@"navbarOn.png"];
        else if (delegate.assetTrackingSession.accuracy > 0 &&  delegate.assetTrackingSession.latField < 0)
            image = [UIImage imageNamed:@"navbarWarning.png"];
        else
            image = [UIImage imageNamed:@"navbarOff.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
}
-(void) viewDidAppear:(BOOL)animated
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    delegate.assetTrackingSession.currentTrackingUnit = nil;
  [_tableView reloadData];   
}

- (void) didTapBackButton:(id)sender {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    return [delegate.assetTrackingSession.units count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AssetTrackingUnitCell";
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    
    NSUInteger row = [indexPath row];
    UITableViewCell *tablecell = nil;
    AssetTrackingUnitCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AssetTrackingUnitCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
    }
    [cell setupData:[delegate.assetTrackingSession.units objectAtIndex:row]];
    tablecell = cell;
    
       
    
   
    
    return tablecell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */
/*
#pragma mark - Table view delegate
-(NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
   AssetTrackingUnitItem *selectitem = [delegate.assetTrackingSession.units objectAtIndex:[indexPath row]];
    for (AssetTrackingUnitItem* item in delegate.assetTrackingSession.units)
    {
        if (item.stopGPSDate != nil)
        {
            if (selectitem == item)
                return indexPath;
            else
                return nil;
        }
    }
    
        return indexPath;
   }
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if (self._tableView.indexPathForSelectedRow != nil)
    {
        
        AssetTrackingUnitItem *item = [delegate.assetTrackingSession.units objectAtIndex:[self._tableView.indexPathForSelectedRow row]];
        delegate.assetTrackingSession.currentTrackingUnit = item;
     //   delegate.assetTrackingSession.selectedOrganisation.beachKey = [NSString stringWithFormat:@"%@%@",delegate.assetTrackingSession.selectedOrganisation.orgID,item.locationPointID];
   //     AssetTrackingTrackMeViewController *ev = (AssetTrackingTrackMeViewController *)[segue destinationViewController];
        
    //    [ev setupData:item];
    }
   
}

@end


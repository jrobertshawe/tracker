//
//  AssetTrackingUnitCell.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/08/12.
//
//

#import "AssetTrackingUnitCell.h"
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CAMediaTimingFunction.h>
@implementation AssetTrackingUnitCell
@synthesize unitName;
@synthesize unitActive;
@synthesize cellBackground;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    if (selected == YES)
     cellBackground.image = [UIImage imageNamed:@"but_navbar_on.png"];
}
-(void)setupData:(AssetTrackingUnitItem *) entityObject
{
    object = entityObject;
    unitName.text = entityObject.locationPointCode;
/*    if (object.stopGPSDate != nil)
    {
        cellBackground.image = [UIImage imageNamed:@"but_navbar_on.png"];
        [self spinLayer:unitActive.layer duration:.7 direction:SPIN_CLOCK_WISE];
    }
    else
    {
        cellBackground.image = [UIImage imageNamed:@"but_navbar_off.png"];
        [unitActive.layer removeAllAnimations];
         unitActive.hidden = YES;
    }
 */
      cellBackground.image = [UIImage imageNamed:@"but_navbar_off.png"];
}

- (void)spinLayer:(CALayer *)inLayer duration:(CFTimeInterval)inDuration
        direction:(int)direction
{
    unitActive.hidden = NO;
    CABasicAnimation* rotationAnimation;
    
    // Rotate about the z axis
    rotationAnimation =
    [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    // Rotate 360 degress, in direction specified
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * direction];
    
    // Perform the rotation over this many seconds
    rotationAnimation.duration = inDuration;
     rotationAnimation.repeatCount = HUGE_VALF; 
    // Set the pacing of the animation
    rotationAnimation.timingFunction =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    // Add animation to the layer and make it so
    [inLayer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

@end
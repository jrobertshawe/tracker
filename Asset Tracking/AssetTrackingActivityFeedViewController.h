//
//  AssetTrackingActivityFeedViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingActivity.h"
@interface AssetTrackingActivityFeedViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic) AssetTrackingActivity *assetTrackingActivity;
@property (strong, nonatomic) MKNetworkOperation *flOperation;
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@end

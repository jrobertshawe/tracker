//
//  AssetTrackingOrgCell.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 6/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingOrganisation.h"

@interface AssetTrackingOrgCell : UITableViewCell{
   NSString* queue;
    AssetTrackingOrganisation *object;
}
@property (strong, nonatomic)   IBOutlet UILabel *orgName;
@property (strong, nonatomic)   IBOutlet UILabel *beachName;
@property (strong, nonatomic)   IBOutlet UILabel *points;
@property (strong, nonatomic)   IBOutlet UIImageView *beachImage;



-(void)setupData:(AssetTrackingOrganisation*) entityObject  queueName:(NSString*) queueName;
- (IBAction)MenuButtonPressed:(id)sender;
@end

//
//  AssetTrackingSecondMenuCellActivity.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 19/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingSecondMenuCellActivity.h"
#import "AssetTrackingAppDelegate.h"
@implementation AssetTrackingSecondMenuCellActivity
@synthesize menuName;
@synthesize lastActivity;
@synthesize lastActivityDate;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupData:(NSString*) menuText
{
    menuName.text  = menuText;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"AssetTrackingSecondMenuCellActivity"
     object:nil];    
}
-(void)handleNotification:(NSNotification *)pNotification
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];

    lastActivity.text = delegate.assetTrackingSession.lastActivity.beachName;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    lastActivityDate.text = [formatter stringFromDate:delegate.assetTrackingSession.lastActivity.createdDate];
  
} 
@end

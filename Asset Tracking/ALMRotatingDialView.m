//
//  ALMRotatingDialView
//
//
//  Created by Jeroen Verbeek on 24/08/12.
//  Copyright (c) 2012 Alive. All rights reserved.
//

#import "ALMRotatingDialView.h"
#import "MathUtility.h"
#import "AssetTrackingAppDelegate.h"
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CAMediaTimingFunction.h>
#pragma mark - Class implementation.


@interface ALMRotatingDialView ()
{
	__weak IBOutlet UIImageView *_dialBack;
	__weak IBOutlet UIView *_dialCover;
	__weak IBOutlet UIImageView *_dialArrow;
    __weak IBOutlet UIImageView *_spinImage;
	__weak IBOutlet UILabel *_dialAngleLabel;
	__weak IBOutlet UILabel *_dialValueLabel;
     __weak IBOutlet UILabel *_finishValueLabel;
      __weak IBOutlet UIButton *_startPauseButton;
	CGPoint _lastResult;
	float _dialAngle;
	float _dialValue;
	float _outerLimit;
	float _innerLimit;
    AssetTrackingUnitItem* object;
   CGSize _dialMidPointOffset;
	CGPoint _dialMidPoint;
	BOOL _touchFinished;
	float _currentAngle;		// Current angle of the dial indicator.
	float _touchStartAngle;		// The angle for the initial touch down.
	float _touchEndAngle;		// The angle after the initial touch down including the touch up.
	CGPoint _startPoint;		// Coordinate for initial touch down.
	CGPoint _endPoint;			// Coordinate for moving and end of touch.

}
@property (weak, nonatomic) IBOutlet UIButton *startPauseButton;
@property (weak, nonatomic) IBOutlet UIImageView *dialBack;
@property (weak, nonatomic) IBOutlet UIView *dialCover;
@property (weak, nonatomic) IBOutlet UIImageView *dialArrow;
@property (weak, nonatomic) IBOutlet UIImageView *spinImage;
@property (weak, nonatomic) IBOutlet UILabel *dialAngleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dialValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *finishValueLabel;

-(void)createView;
-(void)updateDisplay;

@end


@implementation ALMRotatingDialView

@synthesize startPauseButton = _startPauseButton;
@synthesize dialBack = _dialBack;
@synthesize dialCover = _dialCover;
@synthesize dialArrow = _dialArrow;
@synthesize dialAngleLabel = _dialAngleLabel;
@synthesize dialValueLabel = _dialValueLabel;
@synthesize outerLimit = _outerLimit;
@synthesize innerLimit = _innerLimit;

/*---------------------------------------------------------------------------------------------*/
/**
 */
-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
    if(self) {
		[self createView];
    }
    return self;
}


/*---------------------------------------------------------------------------------------------*/
/**
 */
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
		[self createView];
    }
    return self;
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Create the various view parts.
 @author Jeroen Verbeek
 */
-(void)createView
{
	_innerLimit = kDefaultInnerLimit;
	_outerLimit = kDefaultOuterLimit;
	DLog(@"%f, %f", _dialCover.frame.size.width / 2, _dialCover.frame.size.height / 2);
	_dialMidPoint = CGPointMake(140, 140);
	_dialMidPointOffset = CGSizeMake(140, 140);
    
	_dialAngle = 0.0;
	_dialValue = 0.0;
    
	UIView *mid = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5)];
	mid.center = CGPointMake(70, 70);
	mid.backgroundColor = [UIColor redColor];
	[self.dialCover addSubview:mid];
    _touchFinished = NO;
	self.multipleTouchEnabled = NO;
   
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Set the rotation of the dial.
 @param newValue The new value to set the dial too, from 0.0 to 1.0.
 @author Jeroen Verbeek
 */
-(void)customSetDialAngle:(float)newAngle
{
	_dialAngle = newAngle;
	while(_dialAngle < 0)
		_dialAngle += 360.0;
	while(_dialAngle >= 360)
		_dialAngle -= 360.0;
	_currentAngle = _dialAngle * kDegreesToRadiansMult;
	_dialArrow.transform = CGAffineTransformMakeRotation(_currentAngle);
	_dialValue = (_dialAngle / 3.6) * 0.01;
    
    int iSecond = 0;
    
    int iMin = (self.dialAngle / 6);
    
    
    iMin = iMin % 60;
    
    return;
    self.dialValueLabel.text = [NSString stringWithFormat:@"%d:%d", iMin,iSecond];
    
    if (iMin == 0)
    {
        object.stopGPSDate = nil;
        return;
    }


    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterNoStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    _finishValueLabel.text = [formatter stringFromDate:delegate.assetTrackingSession.stopGPSDate ];
    
    DLog(@"a:%f m:%d d:%f ", _currentAngle,iMin,self.dialAngle);
    
    
}
-(void)updateTime:(NSTimeInterval)secondsBetween
{
        
        
        NSInteger ti = (NSInteger)secondsBetween;
        NSInteger seconds = ti % 60;
        NSInteger minutes = (ti / 60) % 60;
           
        self.dialValueLabel.text = [NSString stringWithFormat:@"%02d:%02d", minutes,seconds];

    
}

/*---------------------------------------------------------------------------------------------*/
/**
 @brief Get the current angle for the dial.
 @return Current angle for the dial.
 @author Jeroen Verbeek
 @discussion
 */
-(float)customGetDialAngle
{
	return _dialAngle;
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Set the rotation of the dial.
 @param newValue The new value to set the dial too, should be between 0.0 to 1.0.
 @author Jeroen Verbeek
 */
-(void)customSetDialValue:(float)newValue
{
	_dialAngle = newValue * 3.6;
	_currentAngle = _dialAngle * kDegreesToRadiansMult;
	while(_dialAngle < 0)
		_dialAngle += 360.0;
	while(_dialAngle >= 360)
		_dialAngle -= 360.0;
	_dialAngleLabel.text = [NSString stringWithFormat:@"%f", _dialAngle];
	_dialValue = _dialAngle / 3.6;
	_dialValueLabel.text = [NSString stringWithFormat:@"%f", _dialValue];
	_dialArrow.transform = CGAffineTransformMakeRotation(_currentAngle);
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Get the current angle for the dial.
 @return Current value, should be between 0.0 and 1.0, for the dial.
 @author Jeroen Verbeek
 @discussion
 */
-(float)customGetDialValue
{
	return _dialValue;
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Update the display with wind direction information derived form the user's selection.
 @param point The point where the touch happened.
 @author Jeroen Verbeek
 */
-(void)updateDisplay
{
	CGPoint start, end;
	FindAngleBetween2Lines2D(_startPoint, _dialMidPointOffset, &start);
	FindAngleBetween2Lines2D(_endPoint, _dialMidPointOffset, &end);
    
	DLog(@"s:%f, e:%f, d:%f", start.x, end.x, (end.y - start.y));
	_currentAngle += (end.y - start.y);
	_dialArrow.transform = CGAffineTransformMakeRotation(_currentAngle);
    
	_dialAngle = _currentAngle * MAKE_DEGREE_MULTIPLY;
	while(_dialAngle < 0)
		_dialAngle += 360.0;
	while(_dialAngle >= 360)
		_dialAngle -= 360.0;
//	_dialAngleLabel.text = [NSString stringWithFormat:@"%f", _dialAngle];
	_dialValue = (_dialAngle / 3.6) * 0.01;
//	_dialValueLabel.text = [NSString stringWithFormat:@"%f", _dialValue];
    
   
    int iSecond = 0;
    
    int iMin = (self.dialAngle / 6);
    
    
    iMin = iMin % 60;
   self.dialValueLabel.text = [NSString stringWithFormat:@"%02d:%02d", iMin,iSecond];

    if (iMin == 0)
    {
        object.stopGPSDate = nil;
        return;
    }
    
    object.stopGPSDate = [[NSDate date] dateByAddingTimeInterval:(iMin * 60)];
   
    
    DLog(@"a:%f m:%d d:%f ", _currentAngle,iMin,self.dialAngle);
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	_touchFinished = NO;
	for(UITouch *touch in touches) {
		_startPoint = [touch locationInView:_dialCover];
		_endPoint = _startPoint;
		[self updateDisplay];
		break;
	}
}


-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	if(_touchFinished)
		return;
    
	for(UITouch *touch in touches) {
        
		if(_outerLimit > 0 || _innerLimit > 0) {
			float distance = fabs(FindDistanceBetween2Points2D(_endPoint, _dialMidPoint));
			DLog(@"%d (%f, %f)", (int)distance, [touch locationInView:_dialCover].x, [touch locationInView:_dialCover].y);
			if(distance > _outerLimit || distance < _innerLimit) {
				_touchFinished = YES;
				return;
			}
		}
        
		_startPoint = _endPoint;
		_endPoint = [touch locationInView:_dialCover];
		[self updateDisplay];
        
		break;
	}
}


-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self updateDisplay];	
	_touchFinished = NO;
}


-(void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	[self updateDisplay];
	_touchFinished = NO;
}
-(void)setActive
{
  //    [_dialArrow setImage:[UIImage imageNamed:@"but_circle_on.png"]];
  //  [_dialBack setImage:[UIImage imageNamed:@"img_gps_timer_overlay.png"]];
         [self spinLayer:_spinImage.layer duration:1 direction:1];
 
}
-(void)setInactive
{
    //   [_dialArrow setImage:[UIImage imageNamed:@"but_circle_off.png"]];
   // [_dialBack setImage:[UIImage imageNamed:@"img_gps_timer_overlay2.png"]];
     self.dialValueLabel.text = [NSString stringWithFormat:@"00:00"];
    [_spinImage.layer removeAllAnimations];
        _spinImage.hidden = YES;
}
-(void)setupData:(AssetTrackingUnitItem*)pObject
{
    object = pObject;
}
- (IBAction)ButtonPressed:(id)sender
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if (object.active == YES)
    {
        object.active  = NO;
        delegate.assetTrackingSession.stopGPSDate = nil;
        [self setInactive];
    }
    else
    {

        object.active  = YES;
         [self setActive];
        delegate.assetTrackingSession.stopGPSDate = object.stopGPSDate;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TrackMe" object:object userInfo:nil];
    }
}

- (void)spinLayer:(CALayer *)inLayer duration:(CFTimeInterval)inDuration
        direction:(int)direction
{
    _spinImage.hidden = NO;
    CABasicAnimation* rotationAnimation;
    
    // Rotate about the z axis
    rotationAnimation =
    [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    // Rotate 360 degress, in direction specified
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * direction];
    
    // Perform the rotation over this many seconds
    rotationAnimation.duration = inDuration;
    rotationAnimation.repeatCount = HUGE_VALF;
    // Set the pacing of the animation
    rotationAnimation.timingFunction =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    // Add animation to the layer and make it so
    [inLayer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


@end


#pragma mark END

	
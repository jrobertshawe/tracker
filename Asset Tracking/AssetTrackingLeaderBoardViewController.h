//
//  AssetTrackingLeaderBoardViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 5/07/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingLeaderboard.h"
#import "AssetTrackingAppDelegate.h"

@interface AssetTrackingLeaderBoardViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) MKNetworkOperation *flOperation;
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@end

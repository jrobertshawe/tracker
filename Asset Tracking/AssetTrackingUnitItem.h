//
//  AssetTrackingUnitItem.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 21/08/12.
//
//

#import <Foundation/Foundation.h>

@interface AssetTrackingUnitItem : NSObject
@property (strong, nonatomic) NSNumber *locationPointID;
@property (strong, nonatomic) NSString *locationPointCode;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSDate *updatedDate;
@property (strong, nonatomic) NSString *updateBy;
@property (strong, nonatomic) NSDate* stopGPSDate;
@property bool active;
@end

//
//  AssetTrackingPointCell.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 14/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AssetTrackingItem.h"
@interface AssetTrackingPointCell : UITableViewCell
{
    AssetTrackingItem* entity;
    
}
@property (strong, nonatomic)   IBOutlet UILabel *pointName;
@property (strong, nonatomic)   IBOutlet UILabel *updateBy;
@property (strong, nonatomic)   IBOutlet UILabel *updateDate;
@property (strong, nonatomic)   IBOutlet UIImageView   *sign;
@property (strong, nonatomic)   IBOutlet UIImageView   *cellBackground;
-(void)setupData:(AssetTrackingItem*) entityObject;
- (void)spinLayer:(CALayer *)inLayer duration:(CFTimeInterval)inDuration
        direction:(int)direction;

-(void) showBusy;
-(void) hideBusy;
-(void) CreateNotification:(NSString*)queuename;
-(void)handleNotification:(NSNotification *)pNotification;
@end

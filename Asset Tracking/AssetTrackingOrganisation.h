//
//  AssetTrackingOrganisation.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 4/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssetTrackingOrganisation : NSObject
@property (strong, nonatomic) NSString *orgID;
@property (strong, nonatomic) NSString *orgName;
@property (strong, nonatomic) NSString *orgType;
@property (strong, nonatomic) NSString *beachID;
@property (strong, nonatomic) NSString *beachName;
@property (strong, nonatomic) NSString *beachKey;
@property (strong, nonatomic) NSString *imageFile;
@property (strong, nonatomic) NSString *points;
@property (strong, nonatomic) NSString *rank;
@end

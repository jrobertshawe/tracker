//
//  AssetTrackingAppDelegate.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingAppDelegate.h"




@implementation ALMOperation


@synthesize delegate = _delegate;
@synthesize jsonText = _jsonText;
@synthesize delayTime = _delayTime;
@synthesize jsonDictionary = _jsonDictionary;


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Entry point for the operation.
 @author Jeroen Verbeek
 */
-(void)main
{
	/* Wait timer for delayed request (like live searching). */
	if(_delayTime)
		[NSThread sleepForTimeInterval:_delayTime];

	if(self.isCancelled) {
		if(_delegate && [_delegate respondsToSelector:@selector(operationResult:)]) {
			[_delegate performSelectorOnMainThread:@selector(operationResult:) withObject:nil waitUntilDone:YES];
		}
		return;
	}

	id result = nil;

	if(_jsonDictionary) {
		NSData *jsonData = [NSJSONSerialization dataWithJSONObject:_jsonDictionary options:NSJSONWritingPrettyPrinted error:nil];
		self.jsonText = [NSString stringWithFormat:@"json=%@", [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
		result = [self doRequest];
	}

	if(self.isCancelled) {
		if(_delegate && [_delegate respondsToSelector:@selector(operationResult:)]) {
			[_delegate performSelectorOnMainThread:@selector(operationResult:) withObject:nil waitUntilDone:YES];
		}
		return;
	}

	if(_delegate && [_delegate respondsToSelector:@selector(operationResult:)]) {
		[_delegate performSelectorOnMainThread:@selector(operationResult:) withObject:result waitUntilDone:YES];
	}
	return;
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Perform the operation.
 @return Data returned by the server request.
 @author Jeroen Verbeek
 */
-(id)doRequest
{
	NSError *error;
	NSURLResponse *response;
    NSData *data;
	DLog(@"tracking json %@", _jsonText);
	NSString *trackingURL = [NSString stringWithFormat:@"http://%@/%@", kServerName, @"index.php/points/updateTrackingPoint/"];

	NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:trackingURL]];
	[request setHTTPMethod:@"POST"];

	NSData *postData = [self.jsonText dataUsingEncoding:NSASCIIStringEncoding];
	[request setHTTPBody:postData];

    while(true)
    {
    
	data = [NSURLConnection sendSynchronousRequest:request
										 returningResponse:&response
													 error:&error];

	if(self.isCancelled) {
		if(_delegate && [_delegate respondsToSelector:@selector(operationResult:)]) {
			[_delegate performSelectorOnMainThread:@selector(operationResult:) withObject:nil waitUntilDone:YES];
		}
		return nil;
	}

	NSString *responseData = [[NSString alloc] initWithData:data
												   encoding:NSUTF8StringEncoding];

	DLog(@"response %@ error %d ", responseData, error.code);
	if([responseData isEqualToString:@"Success"] == YES)
        {
            break;
        }
    }
	/* Returing the raw data that came back from the URL request. */
	return data;
}


@end


#pragma mark END









@interface AssetTrackingAppDelegate ()
{
	NSOperationQueue *_uploadQueue;
}

@end


@implementation AssetTrackingAppDelegate
@synthesize managedObjectContext;
@synthesize managedObjectModel;
@synthesize  persistentStoreCoordinator;
@synthesize window = _window;
@synthesize assetTrackingSession;
@synthesize flOperation = _flOperation;
@synthesize assetTrackingLogin = _assetTrackingLogin;
@synthesize networkEngine = _networkEngine;
@synthesize assetTrackingUploadFile =  _assetTrackingUploadFile;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    _networkEngine = [[MKNetworkEngine alloc] initWithHostName:kServerName customHeaderFields:nil];
    
    [_networkEngine useCache];
    self.assetTrackingUploadFile = [[AssetTrackingUploadFile alloc] initWithHostName:kServerName customHeaderFields:nil];
   [self LoadData]; 
    trackingQueue  = [[NSMutableArray alloc] init];
    netLock = [[NSLock alloc] init];
    arrayLock= [[NSLock alloc] init];
       return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    if (self.assetTrackingSession.currentTrackingUnit == nil ||  self.assetTrackingSession.currentTrackingUnit.stopGPSDate == nil )
    {
        [locationManager stopUpdatingLocation];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    if (self.assetTrackingSession != nil)
    {
        [locationManager startUpdatingLocation];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
     
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationPointsActive" object:nil userInfo:nil];
   
}
- (void)uploadFile:(NSString*) fullPath filename:(NSString*) filename 
{
    
    self.flOperation = [self.assetTrackingUploadFile uploadFromFile:fullPath beachKey:assetTrackingSession.selectedOrganisation.beachKey filename:filename];
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        DLog(@"Image uloaded");
            
        
    }
     
     
     
     
                           onError:^(NSError *error) {
                                   
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];
                           }
     ];

    
}
- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(void)StartTracking
{
    assetTrackingSession.accuracy = -1;
     assetTrackingSession.latField = -1;
    if (locationManager == nil)
    {
        locationManager = [[CLLocationManager alloc] init];
       locationManager.delegate = self;
    }
	 [locationManager startUpdatingLocation];
   
}
-(void)StopTracking
{
    [locationManager stopUpdatingLocation];
    
}
- (void)GetTrackingPoints
{
    self.assetTrackingLogin = [[AssetTrackingLogin alloc] initWithHostName:kServerName customHeaderFields:nil];
    //create operation with the host relative path, the params
    //also method (GET,POST,HEAD,etc) and whether you want SSL or not
    
    self.flOperation = [self.assetTrackingLogin getOrgAssets:assetTrackingSession.sessionguid];
    
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UnitsReceived" object:nil userInfo:nil];
        
    }
     
     
     
     
                           onError:^(NSError *error) {
                               
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];
                               [[NSNotificationCenter defaultCenter] postNotificationName:@"UnitsReceived" object:nil userInfo:nil];
                           }
     ];
}

- (void)GetLocationPoints
{
  
    
    self.assetTrackingLogin = [[AssetTrackingLogin alloc] initWithHostName:kServerName customHeaderFields:nil];
    //create operation with the host relative path, the params
    //also method (GET,POST,HEAD,etc) and whether you want SSL or not
    
    self.flOperation = [self.assetTrackingLogin getLocations:assetTrackingSession.sessionguid];
    
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        
         [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationPointsReceived" object:nil userInfo:nil];
        
    }
     
     
     
     
                           onError:^(NSError *error) {
                               
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationPointsReceived" object:nil userInfo:nil];
                           }
     ];
}


#pragma mark - Location manager update delegate


-(void) locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    if(!_uploadQueue) {
		_uploadQueue = [[NSOperationQueue alloc] init];
		/* Only allow one to run at a time - so all run in sequence. */
		_uploadQueue.maxConcurrentOperationCount = 1;
	}




	// NSLog(@"accuracy +/-%gm",newLocation.horizontalAccuracy);
    BOOL isInBackground = NO;
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateBackground){
        isInBackground = YES;
    }
    if (newLocation.coordinate.latitude > 0 )
    {
        assetTrackingSession.longField = 1;
        assetTrackingSession.latField = 1;
        return;
    }
	if (assetTrackingSession != nil) {

        assetTrackingSession.accuracy = newLocation.horizontalAccuracy;
        assetTrackingSession.longField  = newLocation.coordinate.longitude;
        assetTrackingSession.latField  =  newLocation.coordinate.latitude;
        NSTimeInterval diff = [[NSDate date] timeIntervalSinceDate:assetTrackingSession.lastGPSDate];
        if (assetTrackingSession.lastGPSDate != nil && diff < 60) {
          return;
        }
        assetTrackingSession.lastGPSDate = [NSDate date];
    }
    else {
        return;
	}

    if (self.assetTrackingSession.currentTrackingUnit != nil &&
		self.assetTrackingSession.currentTrackingUnit.stopGPSDate != nil &&
		[self.assetTrackingSession.currentTrackingUnit.stopGPSDate compare:[NSDate date]] == NSOrderedDescending) // If the stopTracking date is later than today
	{


		ALMOperation *op = [[ALMOperation alloc] init];
		op.delayTime = 0.0;

        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *dateString = [dateFormat stringFromDate:[NSDate date]];

        op.jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
							 @"26",																				@"locationPointId",
							 [NSString stringWithFormat:@"%f",newLocation.coordinate.longitude],				@"longitude",
							 [NSString stringWithFormat:@"%f",newLocation.coordinate.latitude],					@"latitude",
							 [NSString stringWithFormat:@"Accuracy: %.0fm", newLocation.horizontalAccuracy],	@"description",
							 self.assetTrackingSession.userid,													@"updateBy",
							 self.assetTrackingSession.selectedAssetTrackingOrganisation.orgName,				@"orgName",
							 self.assetTrackingSession.selectedAssetTrackingOrganisation.orgID,					@"orgID",
							 dateString,																		@"updatedDate",
							 self.assetTrackingSession.sessionguid,												@"sessionid",
							 self.assetTrackingSession.currentTrackingUnit.locationPointID,						@"unitId",
							 self.assetTrackingSession.currentTrackingUnit.locationPointCode,					@"locationPointCode",
							 nil, nil];

		[_uploadQueue addOperation:op];








		//        NSURLResponse *response = nil;
		//        NSError *error = nil;

//        NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
//                                                           options:NSJSONWritingPrettyPrinted error:nil];
//
//		NSString *jsonText = [[NSString alloc] initWithData:jsonData
//                                                   encoding:NSUTF8StringEncoding];
//         [arrayLock lock];
//        [trackingQueue insertObject:jsonText atIndex:0];
//       [arrayLock unlock];
//        NSArray * tempArray = trackingQueue;
//        if ([netLock tryLock] == NO)
//            return;
//        for (NSString* postText in tempArray)
//        {
//            DLog(@"tracking json %@",postText);
//            NSString *trackingURL =[ NSString stringWithFormat:@"http://%@/%@",kServerName,@"index.php/points/updateTrackingPoint/"];
//            NSMutableURLRequest*	 request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:trackingURL]];
//            [request setHTTPMethod:@"POST"];
//            NSString* postString = [NSString stringWithFormat:@"json=%@",postText];
//            NSData *postData = [postString dataUsingEncoding:NSASCIIStringEncoding];
//            [request setHTTPBody:postData];
//            
//           
//            
//            NSData *data = [NSURLConnection sendSynchronousRequest:request
//                                                 returningResponse:&response
//                                                             error:&error];
//            
//            
//            NSString *responseData = [[NSString alloc] initWithData:data
//                                                           encoding:NSUTF8StringEncoding];
//            
//             DLog(@"response %@ error %d ",responseData,error.code);
//            if ([responseData isEqualToString:@"Success"] == YES)
//            {
//                [arrayLock lock];
//                [trackingQueue removeObject:postText];
//                [arrayLock unlock];
//
//            }
//        }
//          [netLock unlock];
//
//
//


		
    }
    else
    {
         [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationPointsStop" object:nil userInfo:nil];
       
        if (isInBackground)
        {
            self.assetTrackingSession.currentTrackingUnit.stopGPSDate = nil;
             NSLog(@"Location time is up and now stopping");
             [locationManager stopUpdatingLocation];
        }
    }
  
    return;
    
}
   
#pragma mark -
#pragma mark Core Data stack
-(void)LoadData
{
	
    
	//Core Data Implementation
	NSManagedObjectContext *context = [self managedObjectContext];
    if (!context) {
        // Handle the error.
    }	
}
/**
 Returns the path to the application's Documents directory.
 */
- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}
/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
    
    if (managedObjectContext_ != nil) {
        return managedObjectContext_;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext_ = [[NSManagedObjectContext alloc] init];
        [managedObjectContext_ setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext_;
}


/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
    
    if (managedObjectModel_ != nil) {
        return managedObjectModel_;
    }
    NSString *modelPath = [[NSBundle mainBundle] pathForResource:@"AssetData" ofType:@"momd"];
    NSURL *modelURL = [NSURL fileURLWithPath:modelPath];
    managedObjectModel_ = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return managedObjectModel_;
}


/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (persistentStoreCoordinator_ != nil) {
        return persistentStoreCoordinator_;
    }
    
    NSURL *storeURL = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory] stringByAppendingPathComponent: @"AssetData.sqlite"]];
    
    NSError *error = nil;
    persistentStoreCoordinator_ = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator_ addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
        
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES],NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return persistentStoreCoordinator_;
}
- (void)saveContext
{
    
}
#pragma mark - GPS
- (void)locationUpdate:(CLLocation *)location
{
    if (assetTrackingSession != nil)
    {
        assetTrackingSession.accuracy = location.horizontalAccuracy;
        assetTrackingSession.longField  = location.coordinate.longitude;
        assetTrackingSession.latField  =  location.coordinate.latitude;
        assetTrackingSession.lastGPSDate = [NSDate date];
    }
    
    //	   [self.tableView reloadData];
}

- (void)locationError:(NSError *)error {
}
- (UIImage*)imageWithImage:(UIImage*)sourceImage scaledToSizeWithSameAspectRatio:(CGSize)targetSize
{  
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO) {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor) {
            scaleFactor = widthFactor; // scale to fit height
        }
        else {
            scaleFactor = heightFactor; // scale to fit width
        }
        
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor) {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5; 
        }
        else if (widthFactor < heightFactor) {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }     
    
    CGImageRef imageRef = [sourceImage CGImage];
    CGBitmapInfo bitmapInfo = CGImageGetBitmapInfo(imageRef);
    CGColorSpaceRef colorSpaceInfo = CGImageGetColorSpace(imageRef);
    
    if (bitmapInfo == kCGImageAlphaNone) {
        bitmapInfo = kCGImageAlphaNoneSkipLast;
    }
    
    CGContextRef bitmap;
    
    if (sourceImage.imageOrientation == UIImageOrientationUp || sourceImage.imageOrientation == UIImageOrientationDown) {
        bitmap = CGBitmapContextCreate(NULL, targetWidth, targetHeight, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, kCGImageAlphaNoneSkipLast);
        
    } else {
        bitmap = CGBitmapContextCreate(NULL, targetHeight, targetWidth, CGImageGetBitsPerComponent(imageRef), CGImageGetBytesPerRow(imageRef), colorSpaceInfo, kCGImageAlphaNoneSkipLast);
        
    }   
    
    // In the right or left cases, we need to switch scaledWidth and scaledHeight,
    // and also the thumbnail point
    if (sourceImage.imageOrientation == UIImageOrientationLeft) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, radians(90));
        CGContextTranslateCTM (bitmap, 0, -targetHeight);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationRight) {
        thumbnailPoint = CGPointMake(thumbnailPoint.y, thumbnailPoint.x);
        CGFloat oldScaledWidth = scaledWidth;
        scaledWidth = scaledHeight;
        scaledHeight = oldScaledWidth;
        
        CGContextRotateCTM (bitmap, radians(-90));
        CGContextTranslateCTM (bitmap, -targetWidth, 0);
        
    } else if (sourceImage.imageOrientation == UIImageOrientationUp) {
        // NOTHING
    } else if (sourceImage.imageOrientation == UIImageOrientationDown) {
        CGContextTranslateCTM (bitmap, targetWidth, targetHeight);
        CGContextRotateCTM (bitmap, radians(-180.));
    }
    
    CGContextDrawImage(bitmap, CGRectMake(thumbnailPoint.x, thumbnailPoint.y, scaledWidth, scaledHeight), imageRef);
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage* newImage = [UIImage imageWithCGImage:ref];
    
    CGContextRelease(bitmap);
    CGImageRelease(ref);
    
    return newImage; 
}

@end

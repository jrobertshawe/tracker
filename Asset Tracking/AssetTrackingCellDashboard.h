//
//  AssetTrackingCellDashboard.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 10/07/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetTrackingCellDashboard : UITableViewCell
{
    NSString* leftQueue;
    NSString* rightQueue;
    
}
@property (strong,nonatomic) IBOutlet UIButton* leftButton;
@property (strong,nonatomic) IBOutlet UIButton* rightButton;
-(void)setupData:(NSString*) leftButtonImage rightImage:(NSString*) rightImage leftButtonSelectedImage:(NSString*) leftButtonSelectedImage rightSelectedImage:(NSString*) rightSelectedImage leftQueueName:(NSString*) leftQueueName rightQueueName:(NSString*) rightQueueName;
- (IBAction)LeftMenuButtonPressed:(id)sender;
@end

//
//  AssetTrackingDashBoardViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 14/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingLogin.h"
@interface AssetTrackingDashBoardViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
       
    IBOutlet UIActivityIndicatorView *loggingonActivity;
      IBOutlet UIButton *gpsTracker;
     bool showgpsTracker;
       bool showorgSelection;
} 
@property (strong, nonatomic)  IBOutlet  UIButton *gpsTracker;
@property (strong, nonatomic)  IBOutlet UIActivityIndicatorView *loggingonActivity;
@property (strong, nonatomic)  IBOutlet UITableView		*_tableView;

@property (strong, nonatomic) AssetTrackingLogin *assetTrackingLogin;
@property (strong, nonatomic) MKNetworkOperation *flOperation;
- (void)userLoggedOn;
-(void)handleGPSNotification:(NSNotification *)pNotification;
-(void)handleBeachSafeNotification:(NSNotification *)pNotification;
@end

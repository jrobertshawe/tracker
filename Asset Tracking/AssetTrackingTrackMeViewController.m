//
//  AssetTrackingTrackMeViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 22/08/12.
//
//

#import "AssetTrackingTrackMeViewController.h"
#import "ALMRotatingDialView.h"
#import "AssetTrackingAppDelegate.h"
@interface AssetTrackingTrackMeViewController ()
{
	ALMRotatingDialView *_dialView;
}

@property (readwrite, retain) ALMRotatingDialView *dialView;
@end



@implementation AssetTrackingTrackMeViewController
@synthesize compassBack;
@synthesize compassCover;
@synthesize compassArrow ;
@synthesize compassSelectionName;
-(void)setupData:(AssetTrackingUnitItem*)pObject
{
    object = pObject;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
   
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    
	NSArray *xibObjects;
	ALMRotatingDialView *xibView;
	
		xibObjects = [[NSBundle mainBundle] loadNibNamed:@"ALMRotatingDialView_iPhone" owner:self options:nil];
		xibView = [xibObjects objectAtIndex:0];
		xibView.frame = CGRectMake(0,0,320,416);
	self.dialView = (ALMRotatingDialView*)xibView;
	[self.view addSubview:xibView];
    myButton = [UIButton buttonWithType:UIButtonTypeCustom];
    myButton.frame = CGRectMake(92, 350, 136, 51); // position in the parent view and set the size of the button
    [myButton setTitle:@"Click Me!" forState:UIControlStateNormal];
    // add targets and actions
    [myButton addTarget:self action:@selector(ButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    // add to a view
    [self.view addSubview:myButton];
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotificationLocationActive:)
     name:@"LocationPointsActive"
     object:nil];
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
}
-(void)handleNotificationLocationActive:(NSNotification *)pNotification
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    if (delegate.assetTrackingSession.currentTrackingUnit != nil)
    {
        [self.dialView setActive];
    }
}
-(void)updateTime
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if (delegate.assetTrackingSession.stopGPSDate != nil)
    {
         
        
        NSTimeInterval diff = [delegate.assetTrackingSession.stopGPSDate timeIntervalSinceDate:[NSDate date]];
        if (diff < 0)
        {
             [self stopTimer];
            return;
         }
        
        [self.dialView updateTime:diff];
        int minAngle = (int)(diff/60)*6;
       	[UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             _dialView.dialAngle = minAngle+1;
                         }
                         completion:^(BOOL done){
                         }
         ];
    }
   
}
- (void)stopTimer
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    object.active  = NO;
    [myButton setImage:[UIImage imageNamed:@"but_timer_off.png"] forState:UIControlStateNormal];

    [self.dialView setInactive];
    
    delegate.assetTrackingSession.stopGPSDate = nil;
   
    object.stopGPSDate = nil;
    [self.dialView setInactive];
    if([timer isValid]){
        [timer invalidate];
        timer = nil;
    }
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             _dialView.dialAngle = 0;
                         }
                         completion:^(BOOL done){
                         }];

}
- (IBAction)ButtonPressed:(id)sender
{
     
    if (object.active == YES)
    {
              [self stopTimer];
        
    }
    else
    {
        AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
        
        if ( object.stopGPSDate == nil)
            return;
        delegate.assetTrackingSession.stopGPSDate  = object.stopGPSDate;
        [myButton setImage:[UIImage imageNamed:@"but_timmer_on.png"] forState:UIControlStateNormal];
         delegate.assetTrackingSession.currentTrackingUnit = object;
        object.active  = YES;
        [self.dialView setActive];
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];

       [[NSNotificationCenter defaultCenter] postNotificationName:@"TrackMe" object:object userInfo:nil];
        
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [_dialView setupData:object];
	_dialView.dialAngle = 0.0;
       AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    if (delegate.assetTrackingSession.stopGPSDate != nil)
    {
        [myButton setImage:[UIImage imageNamed:@"but_timmer_on.png"] forState:UIControlStateNormal];
        
        object.active  = YES;
        [self.dialView setActive];
        NSTimeInterval diff = [delegate.assetTrackingSession.stopGPSDate timeIntervalSinceDate:[NSDate date]];
        
        int minAngle = (int)(diff/60)*2;
       	[UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             _dialView.dialAngle = minAngle+1;
                         }
                         completion:^(BOOL done){
                         }
         ];

    }
    else
    {
        [myButton setImage:[UIImage imageNamed:@"but_timer_off.png"] forState:UIControlStateNormal];
        
        object.active  = NO;
        delegate.assetTrackingSession.stopGPSDate = nil;
        [self.dialView setInactive];
    }

    
    
}

- (void) didTapBackButton:(id)sender {
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if (object.active == NO)
        delegate.assetTrackingSession.currentTrackingUnit.stopGPSDate = nil;
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    //killing timer
    if([timer isValid]){
        [timer invalidate];
        timer = nil;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




/*---------------------------------------------------------------------------------------------*/
/**		
 @brief Update the display with wind direction information derived form the user's selection.
 @param point The point where the touch happened.
 @author Jeroen Verbeek
 */
-(void)updateDisplayWithPoint:(CGPoint)point
{
	CGPoint result;
	FindAngleBetween2Lines2D(point, CGSizeMake(140,140), &result);
	float angle = -((_lastResult.y - result.y) - M_PI_2);
	compassBack.transform = CGAffineTransformMakeRotation(angle);
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Handle a tap gesture.
 @param gesture The gesture that caused this call.
 @author Jeroen Verbeek
 */
-(void)handleTap:(UIGestureRecognizer*)gesture
{
	if(gesture == _tap) {
		CGPoint touchPoint = [gesture locationInView:compassBack];
		[self updateDisplayWithPoint:touchPoint];
	}
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Handle a tap gesture.
 @param gesture The gesture that caused this call.
 @author Jeroen Verbeek
 */
-(void)handlePan:(UIGestureRecognizer*)gesture
{
	if(gesture == _pan) {
		CGPoint touchPoint = [gesture locationInView:compassBack];
		[self updateDisplayWithPoint:touchPoint];
	}
}




@end
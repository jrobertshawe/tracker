//
//  AssetTrackingHazard.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 6/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingItem.h"

@implementation AssetTrackingItem
@synthesize locationPointID;
@synthesize locationPointCode;
@synthesize description;
@synthesize updateBy;
@synthesize updatedDate;
@synthesize marked;
@synthesize sentToServer;
@synthesize queueName;
@end

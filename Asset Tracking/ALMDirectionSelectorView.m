//
//  ALMDirectionSelectorView
//  
//
//  Created by Jeroen Verbeek on 24/07/12.
//  Copyright (c) 2012 Alive. All rights reserved.
//

#import "ALMDirectionSelectorView.h"
#import "MathUtility.h"

#pragma mark - Class implementation.


@interface ALMDirectionSelectorView ()
{
	__weak IBOutlet UIImageView *_compassBack;
	__weak IBOutlet UIView *_compassCover;
	__weak IBOutlet UIImageView *_compassArrow;
	__weak IBOutlet UILabel *_compassSelectionName;
	UITapGestureRecognizer *_tap;
	UIPanGestureRecognizer *_pan;
	CGPoint _lastResult;
}

-(void)createView;
-(void)updateDisplayWithPoint:(CGPoint)point;
-(void)handleTap:(UIGestureRecognizer*)gesture;
-(void)handlePan:(UIGestureRecognizer*)gesture;

@end


@implementation ALMDirectionSelectorView


@synthesize compassBack = _compassBack;
@synthesize compassCover = _compassCover;
@synthesize compassArrow = _compassArrow;
@synthesize compassSelectionName = _compassSelectionName;


/*---------------------------------------------------------------------------------------------*/
/**
 */
-(id)initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
    if(self) {
		[self createView];
    }
    return self;
}


/*---------------------------------------------------------------------------------------------*/
/**
 */
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self) {
		[self createView];
    }
    return self;
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Create the various view parts.
 @author Jeroen Verbeek
 */
-(void)createView
{
	_compassSelectionName.text = @"";

	_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
	_tap.numberOfTouchesRequired = 1;
//	[self addGestureRecognizer:_tap];

	_pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
	_pan.maximumNumberOfTouches = 1;
	_pan.minimumNumberOfTouches = 1;
	[self addGestureRecognizer:_pan];
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Update the display with wind direction information derived form the user's selection.
 @param point The point where the touch happened.
 @author Jeroen Verbeek
 */
-(void)updateDisplayWithPoint:(CGPoint)point
{
	CGPoint result;
	FindAngleBetween2Lines2D(point, CGSizeMake(140,140), &result);
	_compassSelectionName.text = [ALMDirectionSelectorView GetCompassNameForAngle:(result.x + 90)];
	float angle = -((_lastResult.y - result.y) - M_PI_2);
	_compassArrow.transform = CGAffineTransformMakeRotation(angle);
   // _lastResult.y  = result.y;
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Handle a tap gesture.
 @param gesture The gesture that caused this call.
 @author Jeroen Verbeek
 */
-(void)handleTap:(UIGestureRecognizer*)gesture
{
	if(gesture == _tap) {
		CGPoint touchPoint = [gesture locationInView:_compassCover];
		[self updateDisplayWithPoint:touchPoint];
	}
}


/*---------------------------------------------------------------------------------------------*/
/**
 @brief Handle a tap gesture.
 @param gesture The gesture that caused this call.
 @author Jeroen Verbeek
 */
-(void)handlePan:(UIGestureRecognizer*)gesture
{
	if(gesture == _pan) {
		CGPoint touchPoint = [gesture locationInView:_compassCover];
		[self updateDisplayWithPoint:touchPoint];
	}
}


/*---------------------------------------------------------------------------------------------*/
/**
 @discussion
 See description in header file.
 */
+(const NSString *)GetCompassNameForAngle:(float)angle
{
	static const NSString *compassNames[33] = {
		@"North",
		@"North by east",
		@"North-northeast",
		@"Northeast by north",
		@"Northeast",
		@"Northeast by east",
		@"East-northeast",
		@"East by north",
		@"East",
		@"East by south",
		@"East-southeast",
		@"Southeast by east",
		@"Southeast",
		@"Southeast by south",
		@"South-southeast",
		@"South by east",
		@"South",
		@"South by west",
		@"South-southwest",
		@"Southwest by south",
		@"Southwest",
		@"Southwest by west",
		@"West-southwest",
		@"West by south",
		@"West",
		@"West by north",
		@"West-northwest",
		@"Northwest by west",
		@"Northwest",
		@"Northwest by north",
		@"North-northwest",
		@"North by west",
		@"North"
	};

	if(angle < 0)
		angle += 360;
	if(angle >= 360)
		angle -= 360;
	float check = angle + 5.625;
	int nameIndex = check / 11.25;

	NSLog(@"%f, %d, %@",angle, nameIndex, compassNames[nameIndex]);
	return compassNames[nameIndex];
}


@end


#pragma mark END



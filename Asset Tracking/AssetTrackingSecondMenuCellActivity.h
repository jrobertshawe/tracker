//
//  AssetTrackingSecondMenuCellActivity.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 19/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetTrackingSecondMenuCellActivity : UITableViewCell

@property (strong, nonatomic)   IBOutlet UILabel *menuName;
@property (strong, nonatomic)   IBOutlet UILabel *lastActivity;
@property (strong, nonatomic)   IBOutlet UILabel *lastActivityDate;
-(void)setupData:(NSString*) menuText;
-(void)handleNotification:(NSNotification *)pNotification;
@end

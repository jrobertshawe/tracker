//
//  AssetTrackingActivity.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingActivity.h"
#import "AssetTrackingAppDelegate.h"

@implementation AssetTrackingActivity
-(MKNetworkOperation*) getActivites
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
      NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                       delegate.assetTrackingSession.sessionguid, @"sessionid",nil];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithPath:@"index.php/activity/getActivities/" 
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];    
    
     
    [op onCompletion:^(MKNetworkOperation *operation) {
        AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
        NSLog(@"AssetTrackingSendPointUpdates:sendToServer response string: %@",[operation responseString]);
        NSArray *responsearray = [operation responseJSON];
        [delegate.assetTrackingSession.activities removeAllObjects];
        for (NSDictionary* dataDict in responsearray)
        {
              AssetTrackingActivityItem* assetTrackingActivityItem  = [[AssetTrackingActivityItem alloc] init];
            assetTrackingActivityItem.recordid    = [dataDict objectForKey:@"id"];
            assetTrackingActivityItem.beachName= [dataDict objectForKey:@"BeachName"];
            assetTrackingActivityItem.itemName= [dataDict objectForKey:@"LocationPointCode"];
            assetTrackingActivityItem.userName = [dataDict objectForKey:@"UserName"];
            assetTrackingActivityItem.imageFile= [dataDict objectForKey:@"PhotoName"];
            NSString *updateddate = [dataDict objectForKey:@"CreatedDate"];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            assetTrackingActivityItem.createdDate = [dateFormatter dateFromString:updateddate];
            [delegate.assetTrackingSession.activities addObject:assetTrackingActivityItem];
            
        }
       
    
  
    } onError:^(NSError *error) {
        
        DLog(@"%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}    
-(MKNetworkOperation*) getLastActivity
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    delegate.assetTrackingSession.sessionguid, @"sessionid",nil];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithPath:@"index.php/activity/getLastActivity" 
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];    
    
    
    [op onCompletion:^(MKNetworkOperation *operation) {
        AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
        
        NSDictionary *dataDict = [operation responseJSON];
        
        AssetTrackingActivityItem* assetTrackingActivityItem  = [[AssetTrackingActivityItem alloc] init];
            assetTrackingActivityItem.recordid    = [dataDict objectForKey:@"id"];
            assetTrackingActivityItem.beachName= [dataDict objectForKey:@"BeachName"];
            assetTrackingActivityItem.itemName= [dataDict objectForKey:@"LocationPointCode"];
            assetTrackingActivityItem.userName = [dataDict objectForKey:@"UserName"];
            assetTrackingActivityItem.orgName = [dataDict objectForKey:@"orgName"];

            NSString *updateddate = [dataDict objectForKey:@"CreatedDate"];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            assetTrackingActivityItem.createdDate = [dateFormatter dateFromString:updateddate];
            delegate.assetTrackingSession.lastActivity  = assetTrackingActivityItem;
            
        
        
        
        
    } onError:^(NSError *error) {
        
        DLog(@"%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}    

@end

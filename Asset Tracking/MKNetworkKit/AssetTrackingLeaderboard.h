//
//  AssetTrackingLeaderboard.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 22/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "MKNetworkEngine.h"
#import "AssetTrackingActivityItem.h"
#import "AssetTrackingLeaderBoardItem.h"
@interface AssetTrackingLeaderboard : MKNetworkEngine
-(MKNetworkOperation*) getLeaders;
-(MKNetworkOperation*) getLeader;
@end

//
//  AssetTrackingSendPointUpdates.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingSendPointUpdates.h"
#import "AssetTrackingAppDelegate.h"
#import "AssetTrackingItem.h"
@implementation AssetTrackingSendPointUpdates
-(MKNetworkOperation*) sendToServer:(AssetTrackingQueueItem*) assetTrackingQueueItem   {
    
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:assetTrackingQueueItem.updatedDate];
    NSMutableDictionary* jsonDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSDictionary dictionaryWithObjectsAndKeys:
                                    assetTrackingQueueItem.locationPointID, @"locationPointId",
                                    assetTrackingQueueItem.longitude, @"longitude",
                                    assetTrackingQueueItem.latitude, @"latitude",
                                    assetTrackingQueueItem.updateBy, @"updateBy",
                                    assetTrackingQueueItem.orgName, @"orgName",
                                    assetTrackingQueueItem.locationPointCode, @"LocationPointCode",
                                    dateString,@"updatedDate",
                                    delegate.assetTrackingSession.sessionguid, @"sessionid",nil]];
    if (delegate.assetTrackingSession.currentTrackingUnit != nil)
        [jsonDictionary setValue:delegate.assetTrackingSession.currentTrackingUnit.locationPointID forKey:@"unit"];
    if (assetTrackingQueueItem.beachID!= nil)
        [jsonDictionary setValue:assetTrackingQueueItem.beachID forKey:@"beachId"];
    if (assetTrackingQueueItem.beachName!= nil)
        [jsonDictionary setValue:assetTrackingQueueItem.beachName forKey:@"beachName"];
    if (assetTrackingQueueItem.clubID!= nil)
        [jsonDictionary setValue:assetTrackingQueueItem.clubID forKey:@"clubId"];
    if (assetTrackingQueueItem.description!= nil)
        [jsonDictionary setValue:assetTrackingQueueItem.description forKey:@"description"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    
   
    assetTrackingQueueItem.status = SENDING;
    
    MKNetworkOperation *op = [self operationWithPath:@"index.php/points/updatePoint/" 
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];    
     DLog(@"%@", jsonText);	
    //[op setUsername:@"bobs@thga.me" password:@"12345678"];	
    
    [op onCompletion:^(MKNetworkOperation *operation) {
        AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
        NSLog(@"AssetTrackingSendPointUpdates:sendToServer response string: %@",[operation responseString]);
        NSDictionary *dataDict = [operation responseJSON];
        NSString *status  = [dataDict objectForKey:@"status"];
        if ([status isEqualToString:@"Done"] == YES)
        {
            [delegate.assetTrackingSession.itemUpdateQueue removeObject:assetTrackingQueueItem];
            for (AssetTrackingItem *item in delegate.assetTrackingSession.allLocationsPoints)
            {
                if ([item.locationPointID intValue] == [assetTrackingQueueItem.locationPointID intValue])
                {
                    item.marked = SENT;
                    item.sentToServer = YES;
                    item.updatedDate = assetTrackingQueueItem.updatedDate;
                    item.updateBy = assetTrackingQueueItem.updateBy;
                      [[NSNotificationCenter defaultCenter] postNotificationName:item.queueName object:nil userInfo:nil];
                    break;
                }
            }
        }
    } onError:^(NSError *error) {
        assetTrackingQueueItem.status = FAILED;
        [delegate.assetTrackingSession.itemUpdateQueue removeObject:assetTrackingQueueItem];
        for (AssetTrackingItem *item in delegate.assetTrackingSession.allLocationsPoints)
        {
            if ([item.locationPointID intValue] == [assetTrackingQueueItem.locationPointID intValue])
            {
                item.marked = FAILED;
                item.sentToServer = NO;
                item.updatedDate = assetTrackingQueueItem.updatedDate;
                item.updateBy = @"Sending to server failed";
                [[NSNotificationCenter defaultCenter] postNotificationName:item.queueName object:nil userInfo:nil];
                break;
            }
        }

        DLog(@"%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end


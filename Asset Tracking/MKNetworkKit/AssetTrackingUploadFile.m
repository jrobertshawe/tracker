	//
//  AssetTrackingUploadFile.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 16/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingUploadFile.h"
#import "AssetTrackingAppDelegate.h"
@implementation AssetTrackingUploadFile

-(MKNetworkOperation*) uploadFromFile:(NSString*) file beachKey:(NSString*) beachKey filename:(NSString*) filename 
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormat stringFromDate:[NSDate date]];
    
   NSMutableDictionary* jsonDictionary = [[NSMutableDictionary alloc] initWithDictionary:[NSDictionary dictionaryWithObjectsAndKeys:
                                    beachKey, @"beach_key",                                                                                    dateString,@"updatedDate",                                                                                    [NSString stringWithFormat:@"%f",delegate.assetTrackingSession.longField], @"longitude",                                                                                          [NSString stringWithFormat:@"%f",delegate.assetTrackingSession.latField], @"latitude",
                                    filename, @"photoName",delegate.assetTrackingSession.sessionguid, @"sessionid", nil]];
    if (delegate.assetTrackingSession.selectedOrganisation.beachID != nil)
        [jsonDictionary setValue:delegate.assetTrackingSession.selectedOrganisation.beachID forKey:@"beachId"];
    if (delegate.assetTrackingSession.selectedOrganisation.beachName!= nil)
        [jsonDictionary setValue:delegate.assetTrackingSession.selectedOrganisation.beachName forKey:@"beachName"];
    if (delegate.assetTrackingSession.selectedOrganisation.orgName!= nil)
        [jsonDictionary setValue:delegate.assetTrackingSession.selectedOrganisation.orgName forKey:@"orgName"];
    if (delegate.assetTrackingSession.selectedOrganisation.orgID!= nil)
        [jsonDictionary setValue:delegate.assetTrackingSession.selectedOrganisation.orgID forKey:@"orgId"];
    
    
    
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    
MKNetworkOperation *op = [self operationWithPath:@"index.php/upload/do_upload" 
                                          params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                  jsonText, @"json",nil]
                                      httpMethod:@"POST"];
 ALog(@"%@", jsonText);
[op addFile:file forKey:@"userfile"];
[op setFreezable:YES];

[op onCompletion:^(MKNetworkOperation* completedOperation) 
 {
     NSString *responseString = [completedOperation responseString];
      ALog(@"%@", responseString);
 
 }
         onError:^(NSError* error) 
 {
     ALog(@" error %d ",error.code);
 }];

[self enqueueOperation:op];
return op;

}
@end
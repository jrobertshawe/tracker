//
//  AssetTrackingLeaderboard.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 22/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingLeaderboard.h"
#import "AssetTrackingAppDelegate.h"
@implementation AssetTrackingLeaderboard
-(MKNetworkOperation*) getLeaders
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    delegate.assetTrackingSession.sessionguid, @"sessionid",nil];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithPath:@"index.php/leader/getLeadboard/" 
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];    
    
    
    [op onCompletion:^(MKNetworkOperation *operation) {
        AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
         NSArray *responsearray = [operation responseJSON];
           DLog(@"%@", [operation responseString]);
        [delegate.assetTrackingSession.leaders removeAllObjects];
        for (NSDictionary* dataDict in responsearray)
        {
            AssetTrackingLeaderBoardItem* assetTrackingLeaderBoardItem  = [[AssetTrackingLeaderBoardItem alloc] init];
            assetTrackingLeaderBoardItem.recordid    = [dataDict objectForKey:@"id"];
            assetTrackingLeaderBoardItem.beachName= [dataDict objectForKey:@"BeachName"];
            assetTrackingLeaderBoardItem.clubName = [dataDict objectForKey:@"orgName"];
            assetTrackingLeaderBoardItem.numberOfUpdates= [dataDict objectForKey:@"numberOfUpdates"];
            assetTrackingLeaderBoardItem.userName = [dataDict objectForKey:@"UserName"];
            assetTrackingLeaderBoardItem.imageFile= [dataDict objectForKey:@"PhotoName"];
            assetTrackingLeaderBoardItem.points= [dataDict objectForKey:@"numberOfUpdates"];
            assetTrackingLeaderBoardItem.rank= [dataDict objectForKey:@"Rank"];
            NSString *updateddate = [dataDict objectForKey:@"LastUpdated"];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            assetTrackingLeaderBoardItem.updatedDate = [dateFormatter dateFromString:updateddate];
            [delegate.assetTrackingSession.leaders addObject:assetTrackingLeaderBoardItem];
            
        }
        
         
        
    } onError:^(NSError *error) {
        
        DLog(@"%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}    
-(MKNetworkOperation*) getLeader
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    delegate.assetTrackingSession.sessionguid, @"sessionid",nil];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithPath:@"index.php/leader/getTopLeader" 
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];    
    
    
    [op onCompletion:^(MKNetworkOperation *operation) {
        AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
         DLog(@"%@", [operation responseString]);
        NSDictionary *dataDict = [operation responseJSON];
        
        AssetTrackingLeaderBoardItem* assetTrackingLeaderBoardItem  = [[AssetTrackingLeaderBoardItem alloc] init];
        assetTrackingLeaderBoardItem.recordid    = [dataDict objectForKey:@"id"];
        assetTrackingLeaderBoardItem.beachName= [dataDict objectForKey:@"BeachName"];
          assetTrackingLeaderBoardItem.numberOfUpdates= [dataDict objectForKey:@"NumberOfUpdates"];
        assetTrackingLeaderBoardItem.userName = [dataDict objectForKey:@"UserName"];
        
        NSString *updateddate = [dataDict objectForKey:@"LastUpdated"];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        assetTrackingLeaderBoardItem.updatedDate = [dateFormatter dateFromString:updateddate];
        delegate.assetTrackingSession.topLeader  = assetTrackingLeaderBoardItem;
        
        
        
        
        
    } onError:^(NSError *error) {
        
        DLog(@"%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}    

@end


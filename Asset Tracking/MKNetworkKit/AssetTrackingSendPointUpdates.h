//
//  AssetTrackingSendPointUpdates.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MKNetworkEngine.h"
#import "AssetTrackingQueueItem.h"

@interface AssetTrackingSendPointUpdates : MKNetworkEngine
-(MKNetworkOperation*) sendToServer:(AssetTrackingQueueItem*) assetTrackingQueueItem;
@end

//
//  AssetTrackingLogin.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface AssetTrackingLogin : MKNetworkEngine
-(MKNetworkOperation*) loginToServer:(NSString*) jsonText password:(NSString*) password  ;
-(MKNetworkOperation*) getOrgs:(NSString*) sessionId;
-(MKNetworkOperation*) getLocations:(NSString*) sessionId ;
-(MKNetworkOperation*) getOrgAssets:(NSString*) sessionId ;
@end

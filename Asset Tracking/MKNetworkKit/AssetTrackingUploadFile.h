//
//  AssetTrackingUploadFile.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 16/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface AssetTrackingUploadFile : MKNetworkEngine
-(MKNetworkOperation*) uploadFromFile:(NSString*) file beachKey:(NSString*) beachKey filename:(NSString*) filename ;
@end

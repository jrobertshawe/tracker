//
//  AssetTrackingActivity.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "MKNetworkEngine.h"
#import "AssetTrackingActivityItem.h"
@interface AssetTrackingActivity : MKNetworkEngine
-(MKNetworkOperation*) getActivites;
-(MKNetworkOperation*) getLastActivity;
@end

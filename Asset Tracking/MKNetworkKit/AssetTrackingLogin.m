//
//  AssetTrackingLogin.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingLogin.h"
#import "AssetTrackingAppDelegate.h"
#import "AssetTrackingSession.h"
#import "AssetTrackingUnitItem.h"
#import <Foundation/NSJSONSerialization.h> 
@implementation AssetTrackingLogin
-(MKNetworkOperation*) loginToServer:(NSString*) userid password:(NSString*) password   {
    
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                   userid, @"userid",
                                   password, @"password", nil];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithPath:@"index.php/login/createSession/" 
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];    
    	
    //[op setUsername:@"bobs@thga.me" password:@"12345678"];	
    
    [op onCompletion:^(MKNetworkOperation *operation) {
        AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
        NSLog(@"login to server response string: %@",[operation responseString]);
        NSDictionary *response = [operation responseJSON];
        delegate.assetTrackingSession.userid = [response objectForKey:@"userID"];
        delegate.assetTrackingSession.sessionguid = [response objectForKey:@"sessionGuid"];
         delegate.assetTrackingSession.firstName = [response objectForKey:@"firstname"];
         delegate.assetTrackingSession.lastName = [response objectForKey:@"surname"];
      
                      
    } onError:^(NSError *error) {
        
        DLog(@"%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}
-(MKNetworkOperation*) getOrgs:(NSString*) sessionId   {
    
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    sessionId, @"sessionid", nil];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithPath:@"index.php/login/GetOrgs/" 
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];    
    
    //[op setUsername:@"bobs@thga.me" password:@"12345678"];	
    
    [op onCompletion:^(MKNetworkOperation *operation) {
         DLog(@"%@", operation.responseString);
         NSArray *responsearray = [operation responseJSON];
            
        for (NSDictionary* dataDict in responsearray)
        {
            AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
            if (dataDict != nil &&  [dataDict isKindOfClass:[NSDictionary class]] && [dataDict count] > 0 )
            {
                AssetTrackingOrganisation* assetTrackingOrganisation  = [[AssetTrackingOrganisation alloc] init];
                assetTrackingOrganisation.orgName= [dataDict objectForKey:@"orgName"];
                assetTrackingOrganisation.orgID= [dataDict objectForKey:@"orgID"];
                assetTrackingOrganisation.orgType= [dataDict objectForKey:@"orgType"];
                assetTrackingOrganisation.beachName= [dataDict objectForKey:@"beachName"];
                assetTrackingOrganisation.beachKey= [dataDict objectForKey:@"beachKey"];
                assetTrackingOrganisation.beachID= [dataDict objectForKey:@"beachID"];
                assetTrackingOrganisation.imageFile= [dataDict objectForKey:@"photoName"];
                assetTrackingOrganisation.points= [dataDict objectForKey:@"numberOfUpdates"];
                assetTrackingOrganisation.rank= [dataDict objectForKey:@"rank"];
                if ([assetTrackingOrganisation.orgType isEqualToString:@"LIFE"] && (assetTrackingOrganisation.beachKey == nil ||  [assetTrackingOrganisation.beachKey isEqual:[NSNull null]]))
                    continue;
               [delegate.assetTrackingSession.organisations addObject:assetTrackingOrganisation];
            }
            
        }
            

         
      
       
        
    } onError:^(NSError *error) {
        
        DLog(@"%@", error);
    }
      
     ];
    
    [self enqueueOperation:op];
    
    return op;
}
-(MKNetworkOperation*) getLocations:(NSString*) sessionId   {
    
    
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSString* beachKey =  delegate.assetTrackingSession.selectedOrganisation.beachKey;
    if (beachKey == nil)
    {
        if (delegate.assetTrackingSession.currentTrackingUnit.locationPointID == nil)
            beachKey = [NSString stringWithFormat:@"%@",delegate.assetTrackingSession.selectedOrganisation.orgID];
        else
           beachKey = [NSString stringWithFormat:@"%@%@",delegate.assetTrackingSession.selectedOrganisation.orgID,delegate.assetTrackingSession.currentTrackingUnit.locationPointID];
        
    }
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    sessionId, @"sessionid",
                                    beachKey, @"beachKey", nil];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData
                                               encoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithPath:@"index.php/login/GetLocationPoints/"
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];
    
    //[op setUsername:@"bobs@thga.me" password:@"12345678"];
    
    [op onCompletion:^(MKNetworkOperation *operation) {
            NSLog(@"getLocations response string: %@",[operation responseString]);
        NSArray *responsearray = [operation responseJSON];
        [delegate.assetTrackingSession.allLocationsPoints removeAllObjects];
        [delegate.assetTrackingSession.hazards removeAllObjects];
        [delegate.assetTrackingSession.assets removeAllObjects];
        [delegate.assetTrackingSession.incidents removeAllObjects];
        
        for (NSDictionary* dataDict in responsearray)
        {
            
            NSString *type = [dataDict objectForKey:@"locationType"];
            if ([type isEqualToString:@"Hazard"] == YES)
            {
                
                AssetTrackingItem* assetTrackingItem  = [AssetTrackingItem alloc];
                assetTrackingItem.updateBy = [dataDict objectForKey:@"updateBy"];
                NSString *updateddate = [dataDict objectForKey:@"updatedDate"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                assetTrackingItem.updatedDate = [dateFormatter dateFromString:updateddate];
                
                
                assetTrackingItem.locationPointCode= [dataDict objectForKey:@"locationPointCode"];
                assetTrackingItem.locationPointID= [dataDict objectForKey:@"locationPointID"];
                assetTrackingItem.description   = [dataDict objectForKey:@"description"];
                [delegate.assetTrackingSession.hazards addObject:assetTrackingItem];
                [delegate.assetTrackingSession.allLocationsPoints addObject:assetTrackingItem];
                
            }
            if ([type isEqualToString:@"Fixed Asset"] == YES)
            {
                
                AssetTrackingItem* assetTrackingItem  = [AssetTrackingItem alloc];
                assetTrackingItem.updateBy = [dataDict objectForKey:@"updateBy"];
                NSString *updateddate = [dataDict objectForKey:@"updatedDate"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                assetTrackingItem.updatedDate = [dateFormatter dateFromString:updateddate];
                
                
                assetTrackingItem.locationPointCode= [dataDict objectForKey:@"locationPointCode"];
                assetTrackingItem.locationPointID= [dataDict objectForKey:@"locationPointID"];
                assetTrackingItem.description   = [dataDict objectForKey:@"description"];
                [delegate.assetTrackingSession.assets addObject:assetTrackingItem];
                [delegate.assetTrackingSession.allLocationsPoints addObject:assetTrackingItem];
                
            }
            if ([type isEqualToString:@"Incident"] == YES)
            {
                
                AssetTrackingItem* assetTrackingItem  = [AssetTrackingItem alloc];
                assetTrackingItem.updateBy = [dataDict objectForKey:@"updateBy"];
                NSString *updateddate = [dataDict objectForKey:@"updatedDate"];
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                assetTrackingItem.updatedDate = [dateFormatter dateFromString:updateddate];
                
                
                assetTrackingItem.locationPointCode= [dataDict objectForKey:@"locationPointCode"];
                assetTrackingItem.locationPointID= [dataDict objectForKey:@"locationPointID"];
                assetTrackingItem.description   = [dataDict objectForKey:@"description"];
                [delegate.assetTrackingSession.incidents addObject:assetTrackingItem];
                [delegate.assetTrackingSession.allLocationsPoints addObject:assetTrackingItem];
            }
            
        }
        
        
        
        
        
        
    } onError:^(NSError *error) {
        
        DLog(@"%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

-(MKNetworkOperation*) getOrgAssets:(NSString*) sessionId   {
    
    
      AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSString* orgID =  delegate.assetTrackingSession.selectedAssetTrackingOrganisation.orgID;
    NSDictionary* jsonDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                    sessionId, @"sessionid",
                                    orgID, @"orgID", nil];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary 
                                                       options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonText = [[NSString alloc] initWithData:jsonData                                        
                                               encoding:NSUTF8StringEncoding];
    MKNetworkOperation *op = [self operationWithPath:@"index.php/login/GetOpsAssets/" 
                                              params:[NSDictionary dictionaryWithObjectsAndKeys:
                                                      jsonText, @"json",nil]
                                          httpMethod:@"POST"];    
    
    //[op setUsername:@"bobs@thga.me" password:@"12345678"];	
    
    [op onCompletion:^(MKNetworkOperation *operation) {
        
        NSLog(@"getOrgAssets response string: %@",[operation responseString]);
        NSArray *responsearray = [operation responseJSON];
        
        for (NSDictionary* dataDict in responsearray)
        {
            if ([[dataDict objectForKey:@"unitID"] isEqual:[NSNull null]])
                continue;
            AssetTrackingUnitItem* assetTrackingUnitItem  = [AssetTrackingUnitItem alloc];
            assetTrackingUnitItem.locationPointID = [dataDict objectForKey:@"unitID"];
            assetTrackingUnitItem.locationPointCode = [dataDict objectForKey:@"unitName"];
            [delegate.assetTrackingSession.units addObject:assetTrackingUnitItem];
        }
   
        
        
        
        
    } onError:^(NSError *error) {
        
        DLog(@"%@", error);
    }];
    
    [self enqueueOperation:op];
    
    return op;
}

@end

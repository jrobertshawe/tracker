//
//  AssetTrackingMainMenuViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingMainMenuViewController.h"
#import "AssetTrackingPointsViewController.h"
#import "AssetTrackingMenuCell.h"
#import "AssetTrackingAppDelegate.h"
#import "AssetTrackingSecondMenuCellLeaderboard.h"
#import "AssetTrackingSecondMenuCellActivity.h"
#import "AssetTrackingPointsViewController.h"
#import <QuartzCore/CAAnimation.h>
#import <QuartzCore/CAMediaTimingFunction.h>
@interface AssetTrackingMainMenuViewController ()

@end

@implementation AssetTrackingMainMenuViewController
@synthesize assetTrackingLogin = _assetTrackingLogin;
@synthesize flOperation = _flOperation;
@synthesize imgPicker;
@synthesize imageView;
@synthesize imageCameraFrameView;
@synthesize assetTrackingActivity;
@synthesize flOperation2 = _flOperation2;
@synthesize assetTrackingLeaderboard;
@synthesize beachName;
@synthesize clubName;
@synthesize busyScreen;
@synthesize points;
@synthesize topButton;
@synthesize unitActive;
@synthesize activeUnitBackground;
@synthesize imageCameraIcon;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    [super viewDidLoad];
    delegate.assetTrackingSession.currentTrackingUnit.stopGPSDate = nil;
    //change back button image
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
     
    self.navigationItem.leftBarButtonItem = backButtonItem;

    if (delegate.assetTrackingSession.selectedOrganisation.imageFile != nil && [delegate.assetTrackingSession.selectedOrganisation.imageFile isKindOfClass:[NSString class]])
    {
        imageCameraIcon.hidden = NO;
        NSRange range = [delegate.assetTrackingSession.selectedOrganisation.imageFile rangeOfString:@"http"
                                                                   options:NSCaseInsensitiveSearch];
        if(range.location != NSNotFound) {
            [delegate.networkEngine imageAtURL:[NSURL URLWithString:delegate.assetTrackingSession.selectedOrganisation.imageFile] onCompletion:^(UIImage *fetchedImage, NSURL *fetchedURL, BOOL isInCache) {
                self.imageView.image = fetchedImage;
            }];
        }
        else
        {
            self.imageView.image = [UIImage imageWithContentsOfFile:delegate.assetTrackingSession.selectedOrganisation.imageFile];
        }
    }
    
    UIImage *image;
    clubName.text =  delegate.assetTrackingSession.selectedOrganisation.orgName;

    if ([delegate.assetTrackingSession.selectedOrganisation.orgType isEqualToString:@"CLUB"] == YES || [delegate.assetTrackingSession.selectedOrganisation.orgType isEqualToString:@"LIFE"] == YES)
    {
        beachName.text =  delegate.assetTrackingSession.selectedOrganisation.beachName;
        if (delegate.assetTrackingSession.selectedOrganisation.rank != nil)
        {
            points.text = [NSString stringWithFormat:@"Ranking: %@ (%@)",delegate.assetTrackingSession.selectedOrganisation.rank,delegate.assetTrackingSession.selectedOrganisation.points];
        }
        self.title = @"Surf Club";
            if ([delegate.assetTrackingSession.selectedOrganisation.orgType isEqualToString:@"LIFE"] == YES)
                    self.title = @"Lifeguard";
        unitActive.hidden = YES;
        activeUnitBackground.hidden = YES;
    }
    else
    {
        activeUnitBackground.hidden = NO;
        if (delegate.assetTrackingSession.currentTrackingUnit != nil &&  delegate.assetTrackingSession.currentTrackingUnit.locationPointCode != nil)
        {
             beachName.hidden = NO;
            beachName.text = delegate.assetTrackingSession.currentTrackingUnit.locationPointCode;
        }
        else
            beachName.hidden = YES;
        imageCameraFrameView.image = [UIImage imageNamed:@"img_support-ops-logo.png"];
        imageView.hidden = YES;
        points.hidden = YES;
        UIImage* btnImage = [UIImage imageNamed:@"but_live-tracker_off.png"];
        [topButton setImage:btnImage forState:UIControlStateNormal];
        btnImage = [UIImage imageNamed:@"but_live-tracker_on.png"];
        [topButton setImage:btnImage forState:UIControlStateHighlighted];
        self.title = @"Support Ops";
        if (delegate.assetTrackingSession.currentTrackingUnit == nil)
        {
            delegate.assetTrackingSession.currentTrackingUnit = [AssetTrackingUnitItem alloc];
            
        }
    }
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        if (delegate.assetTrackingSession.accuracy > 0 && delegate.assetTrackingSession.accuracy < 11 && delegate.assetTrackingSession.latField < 0)
            image = [UIImage imageNamed:@"navbarOn.png"];
        else if (delegate.assetTrackingSession.accuracy > 0 && delegate.assetTrackingSession.latField< 0)
            image = [UIImage imageNamed:@"navbarWarning.png"];
        else            
            image = [UIImage imageNamed:@"navbarOff.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];

        
    }
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotificationLocationPoints:)
     name:@"LocationPointsReceived"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotificationLocationActive:)
     name:@"LocationPointsActive"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotificationLocationStop:)
     name:@"LocationPointsStop"
     object:nil];
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
    if (delegate.assetTrackingSession != nil)
    {
         [delegate StartTracking];
    }
}
-(void)updateTime
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if (delegate.assetTrackingSession.currentTrackingUnit == nil && delegate.assetTrackingSession.currentTrackingUnit.stopGPSDate == nil)
    {
            [unitActive.layer removeAllAnimations];
            unitActive.hidden = YES;
    }

}
-(void)handleNotificationLocationActive:(NSNotification *)pNotification
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    if (delegate.assetTrackingSession.currentTrackingUnit != nil)
    {
        if (delegate.assetTrackingSession.currentTrackingUnit.stopGPSDate != nil)
        {
            unitActive.hidden = NO;
            [self spinLayer:unitActive.layer duration:.7 direction:1];
            timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
            
        }
        else
        {
            [unitActive.layer removeAllAnimations];
            unitActive.hidden = YES;
        }
    }
}
-(void)handleNotificationLocationStop:(NSNotification *)pNotification
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    if (delegate.assetTrackingSession.currentTrackingUnit != nil)
    {
       
            [unitActive.layer removeAllAnimations];
            unitActive.hidden = YES;
    }
}
-(void)handleNotificationLocationPoints:(NSNotification *)pNotification
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    busyScreen.hidden = YES;
    [busyScreen hide];
    if (nextScreen != nil)
    {
        if ([nextScreen isEqualToString:@"TrackMe"] == YES && [delegate.assetTrackingSession.units count] == 0)
            return;
        if (nextScreen != nil && nextScreenSender != nil)
            [self performSegueWithIdentifier:nextScreen sender: nextScreenSender];
    }
}

- (void) didTapBackButton:(id)sender {
    
    
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
     if (delegate.assetTrackingSession.currentTrackingUnit != nil &&  delegate.assetTrackingSession.currentTrackingUnit.stopGPSDate != nil && [delegate.assetTrackingSession.currentTrackingUnit.stopGPSDate compare:[NSDate date]] == NSOrderedDescending) // If the stopTracking date is 
    {
    
    
    UIAlertView *alertWithYesNoButtons = [[UIAlertView alloc] initWithTitle:@"Live Tracker"
                                                       message:@"You have tracking running. Please confirm you want to stop it" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    
    [alertWithYesNoButtons show];
 
    

    
        return;
    }
    
    
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)alertView : (UIAlertView *)alertView clickedButtonAtIndex : (NSInteger)buttonIndex
{
         if(buttonIndex == 0)
        {
            NSLog(@"no button was pressed\n");
        }
        else
        {
            AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
            
            delegate.assetTrackingSession.stopGPSDate = nil;
            NSLog(@"yes button was pressed\n");
            if(self.navigationController.viewControllers.count > 1) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
}
- (IBAction)assetButtonPressed:(id)sender
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if ([delegate.assetTrackingSession.selectedOrganisation.orgType isEqualToString:@"CLUB"] == YES)
         nextScreen = @"UNITS";
    else
        nextScreen = @"UNITS";
    busyScreen.hidden = NO;
    [busyScreen show];
    nextScreenSender = sender;
    [self  GetLocationPoints];
}
- (void)GetLocationPoints
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    
    self.assetTrackingLogin = [[AssetTrackingLogin alloc] initWithHostName:kServerName customHeaderFields:nil];
    //create operation with the host relative path, the params
    //also method (GET,POST,HEAD,etc) and whether you want SSL or not
    
    self.flOperation = [self.assetTrackingLogin getLocations:delegate.assetTrackingSession.sessionguid];
    
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        
        busyScreen.hidden = YES;
        [busyScreen hide];
        if (nextScreen != nil)
        {
            if ([nextScreen isEqualToString:@"TrackMe"] == YES && [delegate.assetTrackingSession.units count] == 0)
                return;
            if (nextScreen != nil && nextScreenSender != nil)
                [self performSegueWithIdentifier:nextScreen sender: nextScreenSender];
        }

        
    }
     
     
     
     
                           onError:^(NSError *error) {
                               
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];
                               [[NSNotificationCenter defaultCenter] postNotificationName:@"LocationPointsReceived" object:nil userInfo:nil];
                           }
     ];
}
- (IBAction)topButtonPressed:(id)sender
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if ([delegate.assetTrackingSession.selectedOrganisation.orgType isEqualToString:@"CLUB"] == YES || [delegate.assetTrackingSession.selectedOrganisation.orgType isEqualToString:@"LIFE"] == YES)
    {
        busyScreen.hidden = NO;
        [busyScreen show];
        nextScreen = @"UNITS";
        nextScreenSender = sender;
        [delegate GetLocationPoints];
        
    }
    else
    {
        if (delegate.assetTrackingSession.currentTrackingUnit != nil)
            [self performSegueWithIdentifier:@"TrackMe" sender: sender];
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
- (IBAction)activityButtonPressed:(id)sender
{
    busyScreen.hidden = NO;
    [busyScreen show];
    self.assetTrackingLeaderboard = [[AssetTrackingLeaderboard alloc] initWithHostName:kServerName customHeaderFields:nil];
          self.assetTrackingActivity = [[AssetTrackingActivity alloc] initWithHostName:kServerName customHeaderFields:nil];
    self.flOperation = [self.assetTrackingActivity getActivites];
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        busyScreen.hidden = YES;
        [busyScreen hide];
        [self performSegueWithIdentifier: @"Activity" sender: self];
        
    }
                           onError:^(NSError *error) {
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];
                           }
     
     ];
    

}
- (IBAction)leaderButtonPressed:(id)sender
{
    busyScreen.hidden = NO;
    [busyScreen show];
    self.assetTrackingLeaderboard = [[AssetTrackingLeaderboard alloc] initWithHostName:kServerName customHeaderFields:nil];
    self.flOperation = [self.assetTrackingLeaderboard getLeaders];
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        busyScreen.hidden = YES;
        [busyScreen hide];
            [self performSegueWithIdentifier: @"LeaderBoard" sender: self];
       
    }
                           onError:^(NSError *error) {
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];
                           }
     
     ];

}
- (IBAction)changeBeachPhoto:(id)sender
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if ([delegate.assetTrackingSession.selectedOrganisation.orgType isEqualToString:@"CLUB"] == YES || [delegate.assetTrackingSession.selectedOrganisation.orgType isEqualToString:@"LIFE"] == YES)
    {
        
        if (delegate.assetTrackingSession.latField  > 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[NSString stringWithFormat:@"Incorrect GPS reading.  Please stand still and try again in 1 min"]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        if (delegate.assetTrackingSession.accuracy < 0|| delegate.assetTrackingSession.accuracy > 110)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:[NSString stringWithFormat:@"The location accuracy is not very good +/-%.0fm.  Please stand still and try again in 1 min",delegate.assetTrackingSession.accuracy]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }

        
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        
        {
            [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        }
        else
        {
            [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        }
        
        [imagePicker setDelegate:self];
        
        [self presentModalViewController:imagePicker animated:YES];
    }

}
-(void) viewDidAppear:(BOOL)animated
{
       AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
   if (delegate.assetTrackingSession.currentTrackingUnit != nil)
   {
        if (delegate.assetTrackingSession.currentTrackingUnit.stopGPSDate != nil)
        {
            unitActive.hidden = NO;
            [self spinLayer:unitActive.layer duration:.7 direction:1];
        }
        else
        {
            [unitActive.layer removeAllAnimations];
            unitActive.hidden = YES;
        }
   }
    

}
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
   
    CGSize newsize;
    newsize.width = 480;
    newsize.height = 480;
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    int w= image.size.width;
    int h= image.size.height;
    NSLog(@"w:%d h:%d",w,h);
    
    
    // if (image.size.height >= newsize.height && image.size.width >= newsize.width)
    ///     imageview.image = [delegate imageWithImage:image scaledToSizeWithSameAspectRatio:newsize];
    //else
    imageView.image = image;
    
    
    
    // Access the uncropped image from info dictionary
    NSString *filename = [NSString stringWithFormat:@"%.0f.jpg",[[NSDate date] timeIntervalSince1970]];
    [self saveImage:image withName:filename];
    [self saveImageSmall:image withName:filename];
    [picker dismissModalViewControllerAnimated:YES];
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
   
    
    //create operation with the host relative path, the params
    //also method (GET,POST,HEAD,etc) and whether you want SSL or not
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,  YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"small_%@",filename]];
    //	[fileManager createFileAtPath:fullPath contents:data attributes:nil];
    [delegate uploadFile:fullPath filename:filename];
     delegate.assetTrackingSession.selectedOrganisation.imageFile = fullPath;
   }
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
	[picker dismissModalViewControllerAnimated:YES];
    
}

-(void)handleNotification:(NSNotification *)pNotification
{
   
}
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    
    
    UIButton *button = (UIButton* )sender;
    DLog(@"%@",segue.identifier);
    if ([segue.identifier isEqualToString:@"LeaderBoard"] != YES && [segue.identifier isEqualToString:@"Activity"] != YES  && [segue.identifier isEqualToString:@"TrackMe"] != YES)
    {
        NSObject *id = [segue destinationViewController];
        AssetTrackingPointsViewController *ev = (AssetTrackingPointsViewController *)[segue destinationViewController];
        if ([ev respondsToSelector:@selector(setupData:)] ) {
        
            [ev setupData:button.tag];
        }   
        //    [ev setupData:objectString];
    }
     if ([segue.identifier isEqualToString:@"TrackMe"] == YES)
     {
         AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
         
         AssetTrackingTrackMeViewController *ev = (AssetTrackingTrackMeViewController *)[segue destinationViewController];
         [ev setupData:delegate.assetTrackingSession.currentTrackingUnit];
     }
    
}
- (void)saveImage:(UIImage *)image withName:(NSString *)name
{
	
	//save image
    //	NSData *data = UIImageJPEGRepresentation(image, 1);
    //	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,  YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:name];
    //	[fileManager createFileAtPath:fullPath contents:data attributes:nil];
    [UIImageJPEGRepresentation(image, 1.0) writeToFile:fullPath atomically:YES];
	
}
- (void)saveImageThumb:(UIImage *)image withName:(NSString *)name
{
    CGSize newsize;
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    newsize.width = 300;
    newsize.height = 300;
    UIImage *resizedimage;
    
    if (image.size.height > newsize.height && image.size.width > newsize.width)
        resizedimage = [delegate imageWithImage:image scaledToSizeWithSameAspectRatio:newsize];
    else
        resizedimage = image;
    
	//save image
	NSString *thumbName = [NSString stringWithFormat:@"thumb_%@",name];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,  YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:thumbName];
    [UIImageJPEGRepresentation(resizedimage, 1.0) writeToFile:fullPath atomically:YES];
    
	
}
- (void)saveImageSmall:(UIImage *)image withName:(NSString *)name
{
    CGSize newsize;
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    newsize.width = 300;
    newsize.height = 300;
    UIImage *resizedimage;
    
    if (image.size.height > newsize.height && image.size.width > newsize.width)
        resizedimage = [delegate imageWithImage:image scaledToSizeWithSameAspectRatio:newsize];
    else
        resizedimage = image;
    
    //save image
	NSString *smallName = [NSString stringWithFormat:@"small_%@",name];
    
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,  YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:smallName];
    [UIImageJPEGRepresentation(resizedimage, 1.0) writeToFile:fullPath atomically:YES];
    
    
    
}
- (void)spinLayer:(CALayer *)inLayer duration:(CFTimeInterval)inDuration
        direction:(int)direction
{
    unitActive.hidden = NO;
    CABasicAnimation* rotationAnimation;
    
    // Rotate about the z axis
    rotationAnimation =
    [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    // Rotate 360 degress, in direction specified
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 * direction];
    
    // Perform the rotation over this many seconds
    rotationAnimation.duration = inDuration;
    rotationAnimation.repeatCount = HUGE_VALF;
    // Set the pacing of the animation
    rotationAnimation.timingFunction =
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    // Add animation to the layer and make it so
    [inLayer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}
@end

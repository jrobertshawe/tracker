//
//  AssetTrackingActivityCell.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingActivityItem.h"
@interface AssetTrackingActivityCell : UITableViewCell

@property (strong, nonatomic)   IBOutlet UILabel *beachName;
@property (strong, nonatomic)   IBOutlet UILabel *userName;
@property (strong, nonatomic)   IBOutlet UILabel *itemName;
@property (strong, nonatomic)   IBOutlet UILabel *createdDate;
@property (strong, nonatomic)   IBOutlet UIImageView *beachImage;
-(void)setupData:(AssetTrackingActivityItem*) assetTrackingActivityItem;
@end

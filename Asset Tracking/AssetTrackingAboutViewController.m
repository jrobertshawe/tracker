//
//  AssetTrackingAboutViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 29/08/12.
//
//

#import "AssetTrackingAboutViewController.h"
#import "AssetTrackingAppDelegate.h"
@interface AssetTrackingAboutViewController ()

@end

@implementation AssetTrackingAboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  	// Do any additional setup after loading the view.
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"img_topbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
}



- (void) didTapBackButton:(id)sender {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(IBAction)makePhoneCall:(id)sender
{
    
        NSString *stringPhoneNumber  = [NSString stringWithFormat:@"telprompt://%@",@"0292158000"];
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringPhoneNumber]];
        } else {
            UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device can not make phone calls." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [Notpermitted show];
   
        }
  }
-(IBAction)sendEmail:(id)sender
{
    
    if ([MFMailComposeViewController canSendMail])
    {
            MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
            picker.mailComposeDelegate = self;
            [picker setSubject:@"SLS Patrol App enquiry"];
            
            NSArray *toRecipients = [NSArray arrayWithObject:@"info@slsa.asn.au"];
            [picker setToRecipients:toRecipients];
            
            
            [self presentModalViewController:picker animated:YES];
    }
}


// Dismisses the email composition interface when users tap Cancel or Send. Proceeds to update the message field with the result of the operation.
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
	NSString *title;
	NSString *alertMessage;
	NSString *yes;
	title = @"Email";
	yes = @"OK";
	
	// open an alert with just an OK button
	
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
			alertMessage = @"Email Dispatch canceled";
			break;
		case MFMailComposeResultSaved:
			alertMessage = @"Email Saved";
			break;
		case MFMailComposeResultSent:
			alertMessage = @"Email Sent";
			break;
		case MFMailComposeResultFailed:
			alertMessage = @"Email failed to send";
			break;
		default:
			alertMessage = @"Email was not sent";
			break;
	}
	
	[self dismissModalViewControllerAnimated:YES];
	
	
}

@end

//
//  AssetTrackingLoginViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingLoginViewController.h"
#import "AssetTrackingAppDelegate.h"

@interface AssetTrackingLoginViewController ()

@end

@implementation AssetTrackingLoginViewController
@synthesize assetTrackingLogin = _assetTrackingLogin;
@synthesize flOperation = _flOperation;
@synthesize userid;
@synthesize password;
@synthesize loggingonActivity;
@synthesize loginButton;
@synthesize busyScreen;
@synthesize helpText;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    loggingonActivity.hidesWhenStopped = YES;
	// Do any additional setup after loading the view.
    //change back button image
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    
    //backButton.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"navbarOff.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];

        
    }
    
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
    helpText.opaque = NO;
    helpText.delegate = self;
    helpText.backgroundColor = [UIColor clearColor];
    [helpText loadHTMLString:@"<a  href='https://portal.sls.com.au' > Forgot details?	 </a>" baseURL:nil];
    
}

- (void) forgotDetails:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://portal.sls.com.au"]];
}
- (void) didTapBackButton:(id)sender {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
-(void) viewWillAppear:(BOOL)animated
{
    loginButton.enabled = YES;
    busyScreen.hidden = YES;
    [busyScreen hide];
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        UIImage *image = [UIImage imageNamed:@"img_topbar.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];

        
    }
    [userid becomeFirstResponder];
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    else
        return YES;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




- (void)GetOrgs
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    [loggingonActivity startAnimating];    
    self.assetTrackingLogin = [[AssetTrackingLogin alloc] initWithHostName:kServerName customHeaderFields:nil];
    //create operation with the host relative path, the params
    //also method (GET,POST,HEAD,etc) and whether you want SSL or not
    
    self.flOperation = [self.assetTrackingLogin getLocations:delegate.assetTrackingSession.sessionguid];
    
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        [loggingonActivity stopAnimating];  
        if ( [delegate.assetTrackingSession.organisations count] == 0)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"No ogranisations"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];    
            loginButton.enabled = YES;
            busyScreen.hidden = YES;
            [busyScreen hide];

            
        }
        else if ( [delegate.assetTrackingSession.organisations count] == 1)
        {
            delegate.assetTrackingSession.selectedOrganisation = [delegate.assetTrackingSession.organisations objectAtIndex:0];
            
       //     [delegate GetLocationPoints];
            [self performSegueWithIdentifier: @"LoggedOn" sender: self];
        }
        else
        {
            [self performSegueWithIdentifier: @"SelectOrgSegue" sender: self];
        }
    }
     
     
     
     
                           onError:^(NSError *error) {
                               [loggingonActivity stopAnimating]; 
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];   
                               loginButton.enabled = YES;
                               busyScreen.hidden = YES;
                               [busyScreen hide];

                           }
     ];
}
- (IBAction)startTracking:(id)sender
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    [delegate StartTracking]; 
}
- (IBAction)stopTracking:(id)sender
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    [delegate StopTracking]; 
}
- (IBAction)loginPressed:(id)sender
{
   [userid resignFirstResponder];
    busyScreen.hidden = NO;
    [busyScreen show];
      AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    delegate.assetTrackingSession = [[AssetTrackingSession alloc] init];
   
    
      self.assetTrackingLogin = [[AssetTrackingLogin alloc] initWithHostName:kServerName customHeaderFields:nil];
    //create operation with the host relative path, the params
    //also method (GET,POST,HEAD,etc) and whether you want SSL or not
 
    self.flOperation = [self.assetTrackingLogin loginToServer:userid.text password:password.text];
     
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        NSDictionary *response = [operation responseJSON];
         
        if ( delegate.assetTrackingSession.sessionguid != nil)
        {
            [self userLoggedOn];
          //  AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
            [delegate StartTracking];

        }
        else
        {
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message: [NSString stringWithFormat:@"Failed to log onto SLS Portal. %@",[response objectForKey:@"message"]]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];

             [userid becomeFirstResponder];
            loginButton.enabled = YES;
            busyScreen.hidden = YES;
            [busyScreen hide];

        }
        
    }     
     
                           onError:^(NSError *error) {
                                [loggingonActivity stopAnimating]; 
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];       
                               loginButton.enabled = YES;
                               busyScreen.hidden = YES;
                               [busyScreen hide];

                           }];
    
   
}
- (void)userLoggedOn
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
   self.assetTrackingLogin = [[AssetTrackingLogin alloc] initWithHostName:kServerName customHeaderFields:nil];
    //create operation with the host relative path, the params
    //also method (GET,POST,HEAD,etc) and whether you want SSL or not
    
    self.flOperation = [self.assetTrackingLogin getOrgs:delegate.assetTrackingSession.sessionguid];
    
    [self.flOperation onCompletion:^(MKNetworkOperation *operation) {
        [loggingonActivity stopAnimating];
        
        if ( [delegate.assetTrackingSession.organisations count] == 0)
        {
            
             [self performSegueWithIdentifier: @"UserLoggedOn" sender: self];
            
            
           /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"No ogranisations"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Dismiss"
                                                  otherButtonTitles:nil];
            [alert show];
            loginButton.enabled = YES;
            busyScreen.hidden = YES;
            [busyScreen hide];
            */

            
        }
        else if ( [delegate.assetTrackingSession.organisations count] == 1)
        {
            delegate.assetTrackingSession.selectedOrganisation = [delegate.assetTrackingSession.organisations objectAtIndex:0];

        
        //    [delegate GetLocationPoints];
                  [self performSegueWithIdentifier: @"UserLoggedOn" sender: self];
        }
        else
        {
              [self performSegueWithIdentifier: @"UserLoggedOn" sender: self];
        }
        
        
             
        loggingonActivity.hidden = YES;
    
    }
     
     
     
     
                           onError:^(NSError *error) {
                               [loggingonActivity stopAnimating];
                               NSLog(@"%@", error);
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                               message:[error localizedDescription]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"Dismiss"
                                                                     otherButtonTitles:nil];
                               [alert show];
                               loginButton.enabled = YES;
                               busyScreen.hidden = YES;
                               [busyScreen hide];

                             
                           }
     ];
    
    
}
@end

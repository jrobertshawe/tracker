//
//  ALMDirectionSelectorView
//  
//
//  Created by Jeroen Verbeek on 24/07/12.
//  Copyright (c) 2012 Alive. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ALMDirectionSelectorView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *compassBack;
@property (weak, nonatomic) IBOutlet UIView *compassCover;
@property (weak, nonatomic) IBOutlet UIImageView *compassArrow;
@property (weak, nonatomic) IBOutlet UILabel *compassSelectionName;

/*---------------------------------------------------------------------------------------------*/
/**
 @brief Get the wind-direction name for a compass direction.
 @param angle The angle to get the name for.
 @return String with name for direction.
 @author Jeroen Verbeek
 @discussion
 Assumes 0 it north, 90 is east.
 */
+(NSString *)GetCompassNameForAngle:(float)angle;

@end

//
//  AssetTrackingCellDashboard.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 10/07/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingCellDashboard.h"

@implementation AssetTrackingCellDashboard
@synthesize leftButton;
@synthesize rightButton;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupData:(NSString*) leftButtonImage rightImage:(NSString*) rightImage leftButtonSelectedImage:(NSString*) leftButtonSelectedImage rightSelectedImage:(NSString*) rightSelectedImage leftQueueName:(NSString*) leftQueueName rightQueueName:(NSString*) rightQueueName

{

    
    UIImage* btnImage = [UIImage imageNamed:leftButtonImage];
    [leftButton setImage:btnImage forState:UIControlStateNormal];
    btnImage = [UIImage imageNamed:leftButtonSelectedImage];
    [leftButton setImage:btnImage forState:UIControlStateHighlighted];
    btnImage = [UIImage imageNamed:rightImage];
    [rightButton setImage:btnImage forState:UIControlStateNormal];
    btnImage = [UIImage imageNamed:rightSelectedImage];
    [rightButton setImage:btnImage forState:UIControlStateHighlighted];
    leftQueue = leftQueueName;
    rightQueue = rightQueueName;
}
- (IBAction)LeftMenuButtonPressed:(id)sender
{
    if (leftQueue != nil)
       [[NSNotificationCenter defaultCenter] postNotificationName:leftQueue object:nil userInfo:nil];
}
- (IBAction)RightMenuButtonPressed:(id)sender
{
    if (rightQueue != nil)
        [[NSNotificationCenter defaultCenter] postNotificationName:rightQueue object:nil userInfo:nil];
}
@end

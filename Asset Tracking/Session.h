//
//  Session.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 7/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Session : NSManagedObject

@property (nonatomic, retain) NSString * name;

@end

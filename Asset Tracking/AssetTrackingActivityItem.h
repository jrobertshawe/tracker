//
//  AssetTrackingActivityItem.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssetTrackingActivityItem : NSObject
@property (strong, nonatomic)  NSString *recordid;
@property (strong, nonatomic) NSString *beachName;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *itemName;
@property (strong, nonatomic) NSDate *createdDate;
@property (strong, nonatomic) NSString *orgName;
@property (strong, nonatomic) NSString *imageFile;
@end

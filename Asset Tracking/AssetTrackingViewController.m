//
//  AssetTrackingViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingViewController.h"
#import "AssetTrackingAppDelegate.h"
@interface AssetTrackingViewController ()

@end

@implementation AssetTrackingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIBarButtonItem *customBarItem;
    UIImage* buttonImage = [UIImage imageNamed:@"but_Login_off.png"];
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
   [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
 //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
   [button setBackgroundImage:[UIImage imageNamed:@"but_login_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	button.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
	[button addTarget:self action:@selector(LoginButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	self.navigationItem.rightBarButtonItem = customBarItem;
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
    [self.view sendSubviewToBack: iPhone5Footer];
  
}



- (void) didTapBackButton:(id)sender {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (IBAction)LoginButtonPressed:(id)sender
{
     [self performSegueWithIdentifier: @"Login" sender: self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)viewWillAppear:(BOOL)animated
{
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    // NSLog(@"%f",version);
    if (version >= 5.0) {
        UIImage *backgroundImage = [UIImage imageNamed:@"img_topbar"];
        [self.navigationController.navigationBar setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    }
    else
    {
        self.title = @"SLSA Portal";
        // UIImage *backgroundImage = [UIImage imageNamed:@"image.png"];
    }
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    [delegate StopTracking];
    

}
@end

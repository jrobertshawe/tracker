//
//  AssetTrackingPointsViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingPointsViewController.h"
#import "AssetTrackingAppDelegate.h"
#import "AssetTrackingQueueItem.h"
#import "AssetTrackingItem.h"
#import "AssetTrackingPointCell.h"
@interface AssetTrackingPointsViewController ()

@end

@implementation AssetTrackingPointsViewController
@synthesize assetTrackingSendPointUpdates = _assetTrackingSendPointUpdates;
@synthesize flOperation = _flOperation;
@synthesize _tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)setupData:(NSUInteger) menuIndexSelected
{
    menuIndex = menuIndexSelected;
    switch (menuIndex)
    {
        case MENUASSET:
            
            self.title = @"Patrol Stuff";
            
            break;
        case MENUHAZARD:
            self.title = @"Hazard";
            break;
        case MENUINCIDENT:
            self.title = @"Incident";
            break;
        default:
            break;
    }
}
- (void)viewDidLoad
{
     AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotification:)
     name:@"AssetTrackingPointsViewController"
     object:nil];    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotificationOther:)
     name:@"AssetTrackingPointsViewControllerOther"
     object:nil];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    
    
    switch (menuIndex)
    {
        case MENUASSET:
            
           self.navigationController.title = @"Asset";
            
            break;
        case MENUHAZARD:
           self.navigationController.title = @"Hazard";
            break;
        case MENUINCIDENT:
            self.navigationController.title = @"Incident";
            break;
        default:
            break;
    }
  
    UIImage *image;
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        if (delegate.assetTrackingSession.accuracy > 0 && delegate.assetTrackingSession.accuracy < 11 && delegate.assetTrackingSession.latField < 0)
            image = [UIImage imageNamed:@"navbarOn.png"];
        else if (delegate.assetTrackingSession.accuracy > 0 && delegate.assetTrackingSession.latField < 0)
            image = [UIImage imageNamed:@"navbarWarning.png"];
        else
            image = [UIImage imageNamed:@"navbarOff.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
}



- (void) didTapBackButton:(id)sender {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)handleNotification:(NSNotification *)pNotification
{

    [self._tableView reloadData];
} 
-(void)handleNotificationOther:(NSNotification *)pNotification
{
    NSString *description = (NSString*)pNotification.object;
    AssetTrackingItem* assetTrackingItem = nil;
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
   
    if (self._tableView.indexPathForSelectedRow != nil)
    {
           NSUInteger row = [self._tableView.indexPathForSelectedRow row];
         if (menuIndex == MENUASSET)
            assetTrackingItem = [delegate.assetTrackingSession.assets objectAtIndex:row];
        else if (menuIndex == MENUHAZARD)
            assetTrackingItem = [delegate.assetTrackingSession.hazards objectAtIndex:row];
        
        else if (menuIndex == MENUINCIDENT)
            assetTrackingItem = [delegate.assetTrackingSession.incidents objectAtIndex:row];
        else return;
        
             
        if (description == nil)
            return;
        if ( assetTrackingItem.marked == SENDING)
            return;
        NSString *queuename = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
        
          AssetTrackingPointCell *cell = (AssetTrackingPointCell*)[self._tableView cellForRowAtIndexPath:self._tableView.indexPathForSelectedRow];
        [cell CreateNotification:queuename];
        [cell showBusy];
         [cell setupData:assetTrackingItem];
        assetTrackingItem.queueName = queuename;
        assetTrackingItem.marked = SENDING;
        assetTrackingItem.updateBy = [NSString stringWithFormat:@"%@ %@",delegate.assetTrackingSession.firstName, delegate.assetTrackingSession.lastName];
        
        assetTrackingItem.updatedDate = [NSDate date];
        assetTrackingItem.sentToServer = NO;
        
        [cell setupData:assetTrackingItem];
        
       
        
        AssetTrackingQueueItem* assetTrackingQueueItem = [AssetTrackingQueueItem alloc];
        assetTrackingQueueItem.longitude    = [NSString stringWithFormat:@"%f",delegate.assetTrackingSession.longField];
        assetTrackingQueueItem.latitude  = [NSString stringWithFormat:@"%f",delegate.assetTrackingSession.latField];
        assetTrackingQueueItem.locationPointID = assetTrackingItem.locationPointID;
        assetTrackingQueueItem.beachID = delegate.assetTrackingSession.selectedOrganisation.beachKey;
        assetTrackingQueueItem.orgName = delegate.assetTrackingSession.selectedOrganisation.orgName;
        assetTrackingQueueItem.clubID = delegate.assetTrackingSession.selectedOrganisation.orgID;
        assetTrackingQueueItem.updateBy = [NSString stringWithFormat:@"%@ %@",delegate.assetTrackingSession.firstName, delegate.assetTrackingSession.lastName];
        if (delegate.assetTrackingSession.selectedOrganisation.beachName == nil)
        {
            if (delegate.assetTrackingSession.currentTrackingUnit.locationPointCode != nil)
                assetTrackingQueueItem.beachName = delegate.assetTrackingSession.currentTrackingUnit.locationPointCode;
            else
                assetTrackingQueueItem.beachName = delegate.assetTrackingSession.selectedOrganisation.beachName;
        }
        if (assetTrackingQueueItem.beachName == nil)
            assetTrackingQueueItem.beachName = delegate.assetTrackingSession.selectedOrganisation.orgName;
        
        assetTrackingQueueItem.description = description;
        assetTrackingQueueItem.locationPointCode = assetTrackingItem.locationPointCode;
        assetTrackingQueueItem.updatedDate = [NSDate date];
        [delegate.assetTrackingSession.itemUpdateQueue addObject:assetTrackingQueueItem];
        self.assetTrackingSendPointUpdates = [[AssetTrackingSendPointUpdates alloc] initWithHostName:kServerName customHeaderFields:nil];
        //create operation with the host relative path, the params
        //also method (GET,POST,HEAD,etc) and whether you want SSL or not
        
        self.flOperation = [self.assetTrackingSendPointUpdates sendToServer:assetTrackingQueueItem];
    }

}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount = 0;
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];

    switch (menuIndex)
    {
        case MENUASSET:            
                     
            rowCount = [delegate.assetTrackingSession.assets count];

            break;
        case MENUHAZARD:            
             rowCount =  [delegate.assetTrackingSession.hazards count];
            break;
        case MENUINCIDENT:            
             rowCount =  [delegate.assetTrackingSession.incidents count];
            break;
        default:
            break;
    }

    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"AssetTrackingPointCell";
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    
    NSUInteger row = [indexPath row];
    UITableViewCell *tablecell = nil;
    AssetTrackingPointCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AssetTrackingPointCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                             reuseIdentifier:CellIdentifier];
    }
    
    tablecell = cell;

       switch (menuIndex)
    {
        case MENUASSET:            
            
            [cell setupData:[delegate.assetTrackingSession.assets objectAtIndex:row]];

            
            break;
        case MENUHAZARD:            
            [cell setupData:[delegate.assetTrackingSession.hazards objectAtIndex:row]];

            break;
        case MENUINCIDENT:            
            [cell setupData:[delegate.assetTrackingSession.incidents objectAtIndex:row]];

            break;
        default:
            break;
    }

       
      tablecell = cell;

    
     return tablecell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];

    if (delegate.assetTrackingSession.latField  > 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:[NSString stringWithFormat:@"Incorrect GPS reading.  Please stand still and try again in 1 min"]
                                                       delegate:nil
                                              cancelButtonTitle:@"Dismiss"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }

    if (delegate.assetTrackingSession.accuracy < 0|| delegate.assetTrackingSession.accuracy > 110)
    {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:[NSString stringWithFormat:@"The location accuracy is not very good +/-%.0fm.  Please stand still and try again in 1 min",delegate.assetTrackingSession.accuracy]
                                                   delegate:nil
                                          cancelButtonTitle:@"Dismiss"
                                          otherButtonTitles:nil];
    [alert show];
        return;
    }
    AssetTrackingPointCell *cell = (AssetTrackingPointCell*)[self._tableView cellForRowAtIndexPath:indexPath];
    NSString *queuename = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];
    
    AssetTrackingItem* assetTrackingItem = nil;
       NSUInteger row = [indexPath row];
    if (menuIndex == MENUASSET)
           assetTrackingItem = [delegate.assetTrackingSession.assets objectAtIndex:row];
    else if (menuIndex == MENUHAZARD)
        assetTrackingItem = [delegate.assetTrackingSession.hazards objectAtIndex:row];
    
    else if (menuIndex == MENUINCIDENT)
        assetTrackingItem = [delegate.assetTrackingSession.incidents objectAtIndex:row];
    else return;

    if ( [assetTrackingItem.locationPointCode isEqualToString:@"Other"] == YES)
    {
        AssetTrackingPointCell *cell = (AssetTrackingPointCell*)[self._tableView cellForRowAtIndexPath:indexPath];
               [self performSegueWithIdentifier: @"Other" sender: self];
        return;
    }
    if ( assetTrackingItem.marked == SENDING)
        return;
    [cell CreateNotification:queuename];
    [cell showBusy];
    assetTrackingItem.queueName = queuename;
    assetTrackingItem.marked = SENDING;
    assetTrackingItem.updateBy = [NSString stringWithFormat:@"%@ %@",delegate.assetTrackingSession.firstName, delegate.assetTrackingSession.lastName];
    
    assetTrackingItem.updatedDate = [NSDate date];
    assetTrackingItem.sentToServer = NO;
   
      [cell setupData:assetTrackingItem];
    
    AssetTrackingQueueItem* assetTrackingQueueItem = [AssetTrackingQueueItem alloc];
    assetTrackingQueueItem.longitude    = [NSString stringWithFormat:@"%f",delegate.assetTrackingSession.longField];
     assetTrackingQueueItem.latitude  = [NSString stringWithFormat:@"%f",delegate.assetTrackingSession.latField];
    assetTrackingQueueItem.locationPointID = assetTrackingItem.locationPointID;
    assetTrackingQueueItem.beachID = delegate.assetTrackingSession.selectedOrganisation.beachKey;
    assetTrackingQueueItem.orgName = delegate.assetTrackingSession.selectedOrganisation.orgName;
    assetTrackingQueueItem.clubID = delegate.assetTrackingSession.selectedOrganisation.orgID;
    assetTrackingQueueItem.updateBy  = [NSString stringWithFormat:@"%@ %@",delegate.assetTrackingSession.firstName, delegate.assetTrackingSession.lastName];
    if (delegate.assetTrackingSession.selectedOrganisation.beachName == nil)
    {
        if (delegate.assetTrackingSession.currentTrackingUnit.locationPointCode != nil)
            assetTrackingQueueItem.beachName = delegate.assetTrackingSession.currentTrackingUnit.locationPointCode;
        else
           assetTrackingQueueItem.beachName = delegate.assetTrackingSession.selectedOrganisation.beachName;
    }
    if (assetTrackingQueueItem.beachName == nil)
        assetTrackingQueueItem.beachName = delegate.assetTrackingSession.selectedOrganisation.orgName;
    
    assetTrackingQueueItem.locationPointCode = assetTrackingItem.locationPointCode;
    assetTrackingQueueItem.locationPointCode = assetTrackingItem.locationPointCode;
    assetTrackingQueueItem.description = @"";
    assetTrackingQueueItem.updatedDate = [NSDate date];
   [delegate.assetTrackingSession.itemUpdateQueue addObject:assetTrackingQueueItem];
    self.assetTrackingSendPointUpdates = [[AssetTrackingSendPointUpdates alloc] initWithHostName:kServerName customHeaderFields:nil];
    //create operation with the host relative path, the params
    //also method (GET,POST,HEAD,etc) and whether you want SSL or not
    
    self.flOperation = [self.assetTrackingSendPointUpdates sendToServer:assetTrackingQueueItem];
    
}

@end

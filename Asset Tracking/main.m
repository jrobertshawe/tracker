//
//  main.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AssetTrackingAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AssetTrackingAppDelegate class]));
    }
}

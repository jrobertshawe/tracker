//
//  AssetTrackingUnitCell.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 20/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AssetTrackingUnitItem.h"
#define SPIN_CLOCK_WISE 1
#define SPIN_COUNTERCLOCK_WISE -1
@interface AssetTrackingUnitCell : UITableViewCell
{
    AssetTrackingUnitItem* object;
}
@property (strong, nonatomic)   IBOutlet UILabel *unitName;
@property (strong, nonatomic)   IBOutlet UIImageView *unitActive;
@property (strong, nonatomic)   IBOutlet UIImageView *cellBackground;
-(void)setupData:(AssetTrackingUnitItem *) entityObject;
- (IBAction)ButtonPressed:(id)sender;

@end

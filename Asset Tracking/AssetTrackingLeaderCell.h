//
//  AssetTrackingLeaderCell.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 6/07/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingLeaderBoardItem.h"
@interface AssetTrackingLeaderCell : UITableViewCell
@property (strong, nonatomic)   IBOutlet UILabel *clubName;
@property (strong, nonatomic)   IBOutlet UILabel *beachName;
@property (strong, nonatomic)   IBOutlet UILabel *userName;
@property (strong, nonatomic)   IBOutlet UILabel *ranking;
@property (strong, nonatomic)   IBOutlet UILabel *updatedDate;
@property (strong, nonatomic)   IBOutlet UIImageView *beachImage;
-(void)setupData:(AssetTrackingLeaderBoardItem*) assetTrackingLeaderBoardItem;
@end

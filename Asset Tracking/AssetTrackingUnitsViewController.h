//
//  AssetTrackingUnitsViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 21/08/12.
//
//

#import <UIKit/UIKit.h>
#import "AssetTrackingUnitItem.h"
#import "AssetTrackingTrackMeViewController.h"
#import "AssetTrackingBusyView.h"
@interface AssetTrackingUnitsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
-(void)handleRefresh:(NSNotification *)pNotification;

@end

//
//  AssetTrackingUnitItem.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 21/08/12.
//
//

#import "AssetTrackingUnitItem.h"

@implementation AssetTrackingUnitItem
@synthesize locationPointID;
@synthesize locationPointCode;
@synthesize description;
@synthesize updateBy;
@synthesize updatedDate;
@synthesize active;
@synthesize stopGPSDate;
@end

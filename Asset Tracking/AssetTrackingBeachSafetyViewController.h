//
//  AssetTrackingBeachSafetyViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 30/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AssetTrackingBeachSafetyViewController : UIViewController
{
    IBOutlet UIWebView *      webView;
}
@property (nonatomic, retain)  IBOutlet UIWebView *      webView;
@end

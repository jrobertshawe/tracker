//
//  AssetTrackingItemQueue.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingQueueItem.h"

@implementation AssetTrackingQueueItem
@synthesize  locationPointID;
@synthesize  beachID;
@synthesize  clubID;
@synthesize  longitude;
@synthesize  latitude;
@synthesize  description;
@synthesize  updatedDate;
@synthesize  updateBy;
@synthesize status;
@synthesize beachName;
@synthesize orgName;
@synthesize locationPointCode;
@end

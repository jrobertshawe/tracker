//
//  AssetTrackingLeaderCell.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 6/07/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingLeaderCell.h"
#import "AssetTrackingAppDelegate.h"
@implementation AssetTrackingLeaderCell
@synthesize beachName;
@synthesize userName;
@synthesize ranking;
@synthesize updatedDate;
@synthesize beachImage;
@synthesize clubName;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setupData:(AssetTrackingLeaderBoardItem*) assetTrackingLeaderBoardItem
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    beachName.text = assetTrackingLeaderBoardItem.beachName;
    userName.text = assetTrackingLeaderBoardItem.userName;
    ranking.text = [NSString stringWithFormat:@"%@ (%@)",assetTrackingLeaderBoardItem.rank,assetTrackingLeaderBoardItem.points];
    
    clubName.text = assetTrackingLeaderBoardItem.clubName;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateStyle:NSDateFormatterMediumStyle];
	[formatter setTimeStyle:NSDateFormatterMediumStyle];
    updatedDate.text = [formatter stringFromDate:assetTrackingLeaderBoardItem.updatedDate];

    
    NSRange range = [assetTrackingLeaderBoardItem.imageFile rangeOfString:@"http"
                                                  options:NSCaseInsensitiveSearch];
    if(range.location != NSNotFound) {
        [delegate.networkEngine imageAtURL:[NSURL URLWithString:assetTrackingLeaderBoardItem.imageFile] onCompletion:^(UIImage *fetchedImage, NSURL *fetchedURL, BOOL isInCache) {
            self.beachImage.image = fetchedImage;
        }];
    }
    else
    {
        self.beachImage.image = [UIImage imageWithContentsOfFile:assetTrackingLeaderBoardItem.imageFile];
    }
    
    
}
@end

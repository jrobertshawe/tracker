//
//  AssetTrackingLeaderBoardItem.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 5/07/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingLeaderBoardItem.h"

@implementation AssetTrackingLeaderBoardItem
@synthesize recordid;
@synthesize beachName;
@synthesize userName;
@synthesize numberOfUpdates;
@synthesize updatedDate;
@synthesize imageFile;
@synthesize points;
@synthesize rank;
@synthesize clubName;
@end

//
//  AssetTrackingAboutViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 29/08/12.
//
//
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import <UIKit/UIKit.h>

@interface AssetTrackingAboutViewController : UIViewController<MFMailComposeViewControllerDelegate>
-(IBAction)makePhoneCall:(id)sender;
-(IBAction)sendEmail:(id)sender;

@end

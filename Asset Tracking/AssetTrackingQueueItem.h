//
//  AssetTrackingItemQueue.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AssetTrackingQueueItem : NSObject
@property (strong, nonatomic) NSNumber *locationPointID;
@property (strong, nonatomic) NSString *beachID;
@property (strong, nonatomic) NSString *beachName;
@property (strong, nonatomic) NSString *clubID;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSDate *updatedDate;
@property (strong, nonatomic) NSString *updateBy;
@property int status;
@property (strong, nonatomic) NSString *orgName;
@property (strong, nonatomic) NSString *locationPointCode;
@end

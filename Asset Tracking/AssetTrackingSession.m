//
//  AssetTrackingSession.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 1/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AssetTrackingSession.h"

@implementation AssetTrackingSession
@synthesize userid;
@synthesize password;
@synthesize sessionguid;
@synthesize firstName;
@synthesize lastName;
@synthesize organisations;
@synthesize assets;
@synthesize hazards;
@synthesize incidents;
@synthesize itemUpdateQueue;
@synthesize latField;
@synthesize longField;
@synthesize accuracy;
@synthesize clubId;
@synthesize beachId;
@synthesize allLocationsPoints;
@synthesize selectedOrganisation;
@synthesize activities;
@synthesize lastActivity;
@synthesize stopTracking;
@synthesize topLeader;
@synthesize leaders;
@synthesize units;
@synthesize selectedAssetTrackingOrganisation;
@synthesize stopGPSDate;
@synthesize currentTrackingUnit;
-(AssetTrackingSession*) init
{
    organisations = [[NSMutableArray alloc] init];
  
    assets = [[NSMutableArray alloc] init];
   
    hazards = [[NSMutableArray alloc] init];
   
    incidents = [[NSMutableArray alloc] init];
   
    itemUpdateQueue= [[NSMutableArray alloc] init];
    
    allLocationsPoints = [[NSMutableArray alloc] init];

    activities = [[NSMutableArray alloc] init];
    
    units = [[NSMutableArray alloc] init];
    
    leaders =  [[NSMutableArray alloc] init];

    currentTrackingUnit = nil;
    return self;
}
@end

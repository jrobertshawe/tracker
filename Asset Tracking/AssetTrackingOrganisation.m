//
//  AssetTrackingOrganisation.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 4/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingOrganisation.h"

@implementation AssetTrackingOrganisation
@synthesize orgID;
@synthesize orgName;
@synthesize orgType;
@synthesize beachID;
@synthesize beachName;
@synthesize beachKey;
@synthesize imageFile;
@synthesize points;
@synthesize rank;
@end

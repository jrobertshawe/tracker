//
//  AssetTrackingSelectOrgViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 6/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingLogin.h"
#import "AssetTrackingBusyView.h"
@interface AssetTrackingSelectOrgViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) IBOutlet UITableView		*_tableView;
@property (strong, nonatomic) AssetTrackingLogin *assetTrackingLogin;
@property (strong, nonatomic) MKNetworkOperation *flOperation;
@property (strong,nonatomic) IBOutlet AssetTrackingBusyView* busyScreen;
-(void)handleNotificationLocationPoints:(NSNotification *)pNotification;
-(void)handleOpsSelection:(NSNotification *)pNotification;
-(void)handleBeachSelection:(NSNotification *)pNotification;
@end

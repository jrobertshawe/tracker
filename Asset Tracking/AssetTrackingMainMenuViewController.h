//
//  AssetTrackingMainMenuViewController.h
//  Asset Tracking
//
//  Created by Julian robertshawe on 11/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AssetTrackingLogin.h"
#import "AssetTrackingUploadFile.h"
#import "AssetTrackingActivity.h"
#import "AssetTrackingLeaderboard.h"
#import "AssetTrackingBusyView.h"
#import "AssetTrackingTrackMeViewController.h"
@interface AssetTrackingMainMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate >
{
    	IBOutlet UITableView		*_tableView;
    IBOutlet UITableView		*_secondtableView;
    UIImagePickerController *imgPicker;
    NSString *nextScreen;
     NSObject *nextScreenSender;
    NSTimer *timer;

}
  @property (strong, nonatomic) AssetTrackingActivity *assetTrackingActivity;
    @property (strong, nonatomic) AssetTrackingLeaderboard *assetTrackingLeaderboard;
@property (strong, nonatomic) AssetTrackingLogin *assetTrackingLogin;

@property (strong, nonatomic) MKNetworkOperation *flOperation;
@property (strong, nonatomic) MKNetworkOperation *flOperation2;
@property (strong, retain) UIImagePickerController *imgPicker;
@property (strong, retain) IBOutlet UIImageView *imageView;
@property (strong, retain) IBOutlet UIImageView *imageCameraFrameView;
@property (strong, retain) IBOutlet UIImageView *imageCameraIcon;
@property (strong, retain) IBOutlet UIButton *topButton;
@property (strong, retain) IBOutlet UILabel *beachName;
@property (strong, retain) IBOutlet UILabel *clubName;
@property (strong, retain) IBOutlet UILabel *points;
@property (strong,nonatomic) IBOutlet AssetTrackingBusyView* busyScreen;
@property (strong, nonatomic)   IBOutlet UIImageView *unitActive;
@property (strong, retain) IBOutlet UIImageView *activeUnitBackground;
- (IBAction)leaderButtonPressed:(id)sender;
- (IBAction)changeBeachPhoto:(id)sender;
- (IBAction)assetButtonPressed:(id)sender;
- (IBAction)activityButtonPressed:(id)sender;
- (IBAction)topButtonPressed:(id)sender;
-(void)handleNotificationLocationPoints:(NSNotification *)pNotification;
-(void)updateTime;
@end

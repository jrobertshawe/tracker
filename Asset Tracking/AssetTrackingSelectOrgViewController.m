//
//  AssetTrackingSelectOrgViewController.m
//  Asset Tracking
//
//  Created by Julian robertshawe on 6/06/12.
//  Copyright (c) 2012 Alive Mobile. All rights reserved.
//

#import "AssetTrackingSelectOrgViewController.h"
#import "AssetTrackingAppDelegate.h"
#import "AssetTrackingOrgCell.h"
#import "AssetTrackingOrganisation.h"
#import "AssetTrackingMainMenuViewController.h"
@interface AssetTrackingSelectOrgViewController ()

@end

@implementation AssetTrackingSelectOrgViewController
@synthesize _tableView;
@synthesize assetTrackingLogin = _assetTrackingLogin;
@synthesize flOperation = _flOperation;
@synthesize busyScreen;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage* buttonImage = [UIImage imageNamed:@"but_back_off.png"];
    [backButton setBackgroundImage:buttonImage forState:UIControlStateNormal];
    //   buttonImage = [UIImage imageNamed:@"but_Login_on.png"];
    [backButton setBackgroundImage:[UIImage imageNamed:@"but_back_select.png"] forState:UIControlStateHighlighted];
	//set the frame of the button to the size of the image (see note below)
	backButton.frame = CGRectMake(0, 0, 64, 33);//buttonImage.size.width, buttonImage.size.height);
    
    [backButton addTarget:self action:@selector(didTapBackButton:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItem = backButtonItem;
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleBeachSelection:)
     name:@"BeachSelection"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleOpsSelection:)
     name:@"SpecialSelection"
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(handleNotificationLocationPoints:)
     name:@"UnitsReceived"
     object:nil];
    
    UIImage *image;
    
    if ([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        if (delegate.assetTrackingSession.accuracy > 0 && delegate.assetTrackingSession.accuracy < 11 &&  delegate.assetTrackingSession.latField < 0)
            image = [UIImage imageNamed:@"navbarOn.png"];
        else if (delegate.assetTrackingSession.accuracy > 0 &&  delegate.assetTrackingSession.latField < 0)
            image = [UIImage imageNamed:@"navbarWarning.png"];
        else
            image = [UIImage imageNamed:@"navbarOff.png"];
        [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
        
        
    }
      
    UIImageView *iPhone5Footer = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"img_iphone5.png"]];
    [iPhone5Footer setFrame:CGRectMake(0, FOOTERYPOS, 320, FOOTERHEIGHT)];
    [self.view addSubview:iPhone5Footer];
}
- (void) didTapBackButton:(id)sender {
    if(self.navigationController.viewControllers.count > 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(void) viewWillAppear:(BOOL)animated
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    delegate.assetTrackingSession.currentTrackingUnit = nil;
    [_tableView reloadData];
}
-(void)handleBeachSelection:(NSNotification *)pNotification
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    NSObject *object = pNotification.object;
    delegate.assetTrackingSession.selectedOrganisation = (AssetTrackingOrganisation*) object;
     // [delegate GetLocationPoints];
     [self performSegueWithIdentifier: @"ShowBeachMenu" sender: self];
    
     
    
}

-(void)handleNotificationLocationPoints:(NSNotification *)pNotification
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    if ([delegate.assetTrackingSession.units count] == 0)
        [self performSegueWithIdentifier: @"ShowBeachMenu" sender: self];
    else
         [self performSegueWithIdentifier: @"SelectUnit" sender: self];
    
    
    //  [delegate.assetTrackingSession.units removeAllObjects];
    //        [self performSegueWithIdentifier: @"ShowBeachMenu" sender: self];
    
    
}
-(void)handleOpsSelection:(NSNotification *)pNotification
{
   
    
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSObject *object = pNotification.object;
    delegate.assetTrackingSession.selectedAssetTrackingOrganisation = (AssetTrackingOrganisation*) object;
      delegate.assetTrackingSession.selectedOrganisation = (AssetTrackingOrganisation*) object;
    [delegate.assetTrackingSession.units removeAllObjects];
    [delegate GetTrackingPoints];
    
    
    //  [delegate.assetTrackingSession.units removeAllObjects];
    //        [self performSegueWithIdentifier: @"ShowBeachMenu" sender: self];
    
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    
    return [delegate.assetTrackingSession.organisations count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = @"AssetTrackingBeachOrgCell";
    AssetTrackingAppDelegate *delegate = (AssetTrackingAppDelegate* )[[UIApplication sharedApplication] delegate];
    NSString* queueName;
    
    NSUInteger row = [indexPath row];
	   UITableViewCell *tablecell = nil;
    
     AssetTrackingOrganisation *org = [delegate.assetTrackingSession.organisations objectAtIndex:row];
     if ([org.orgType isEqualToString:@"CLUB"] == YES || [org.orgType isEqualToString:@"LIFE"]  )
     {
         CellIdentifier = @"AssetTrackingBeachOrgCell";
         queueName = @"BeachSelection";
         
     }
     else   if ([org.orgType isEqualToString:@"OPS"] == YES)
     {
         CellIdentifier = @"AssetTrackingSpecialOrgCell";
          queueName = @"SpecialSelection";
     }
        
     AssetTrackingOrgCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AssetTrackingOrgCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                        reuseIdentifier:CellIdentifier];
    }
    [cell setupData:[delegate.assetTrackingSession.organisations objectAtIndex:row] queueName:queueName];
    tablecell = cell;
    
    
    return tablecell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
}

@end

//
//  MathUtility.m
//  AliveExamples
//
//  Created by Jeroen Verbeek on 9/08/12.
//  Copyright (c) 2012 Alive. All rights reserved.
//

#import "MathUtility.h"


/*---------------------------------------------------------------------------------------------*/
/**
 @discussion
 See description in header file.
 */
float FindDistanceBetween2Points2D(CGPoint one, CGPoint two)
{
	CGPoint dist;
	dist.x = one.x - two.x;
	dist.y = one.y - two.y;
	return sqrtf((dist.x * dist.x) + (dist.y * dist.y));
}


/*---------------------------------------------------------------------------------------------*/
/**
 @discussion
 See description in header file.
 */
void FindAngleBetween2Lines2D(CGPoint point, CGSize offset, CGPoint *result)
{
	float dx = point.x - offset.width;
	float dy = point.y - offset.height;
	float radians = atan2f(dy,dx);
	float degrees = radians * MAKE_DEGREE_MULTIPLY;
	if(degrees < 0)
		degrees += 360;
	result->x = degrees;
	result->y = radians;
}


#pragma mark END

